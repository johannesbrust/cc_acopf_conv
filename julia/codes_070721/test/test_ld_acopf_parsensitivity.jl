#------------- LD-AC-OPF (Load Dynamic AC-Optimal Power Flow) -----------------#
# test_ld_acopf_parsensitivity.jl is a script to evaluate the  ld_acopf model
# for different parameter inputs.
#
#
#-------------------------------------------------------------------------------
# 09/04/19, J.B.

const path = pwd() * "/cases/"
const case = "case118" #case9 case30 case118

## -----------------------------------------------------------------------------
## environment
## -----------------------------------------------------------------------------
import Pkg
Pkg.activate(dirname(dirname(dirname(path))))
Pkg.instantiate()
#using Test
using MPCCases, Printf, MAT
using JuMP, JuMPUtil, Ipopt, MathProgBase
using SparseArrays, LinearAlgebra, Distributions
using DelimitedFiles

#using OPF

include("../src/OPF.jl")

### Initialize data
opfdata     = load_case(case, path, other=false);
#epsv        = [0; 1e-4];
epsv        = [0; 1e-4; 1e-3; 1e-2; 1e-1];

### Load Dynamic model
ld_m            = OPF.ld_acopf_model(opfdata);

lddata,rb,rg,rm = OPF.ld_acopf_parsensitivity(ld_m,opfdata,epsv);

open("data_ld.txt", "w") do io
    writedlm(io,lddata);
end

open("data_rld.txt", "w") do io
    writedlm(io,[rb rg rm]);
end

open("data_eps.txt", "w") do io
    writedlm(io,epsv);
end

#plot(epsv,[lddata[rb[:],5],lddata[rg[:],5]])

# plot(tics,tracker,label = ["ModelC cost" "ACOPF cost"], lw = 3, marker = (:circle,2))
# title!("Cost vs. Uncertainty Budget (30 Bus): Sigma_d")
# xlabel!("Sigma_d")
# ylabel!("Objective function")
