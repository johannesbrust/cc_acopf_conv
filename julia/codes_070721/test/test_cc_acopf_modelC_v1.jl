######## CC-AC-OPF (Chance-Constrained AC-Optimal Power Flow) ##################
# Formulation of probability constrained optimal power flow (OPF) problems.
#
# test_cc_acopf_modelC_v1.jl tests the implementation of modelC_v1, an iterative
# approach to the cc problem

################################################################################
# 06/11/19, E.G.


const path = pwd() * "/cases/"
const case = "case300" #case9 case30 case118 case300 case1354pegase case2383wp
# case2736sp # case2737sop

## -----------------------------------------------------------------------------
## environment
## -----------------------------------------------------------------------------
import Pkg
Pkg.activate(dirname(dirname(dirname(path))))
Pkg.instantiate()

using Test
using MPCCases, Printf, MAT
using JuMP, JuMPUtil, Ipopt, MathProgBase
using SparseArrays, LinearAlgebra, Distributions
#using OPF

include("../src/OPF.jl")
#include("../src/jacobian_modelB_v1.jl")
#include("../src/cc_acopf_modelB_v1.jl")

opfdata     = load_case(case, path, other=false);

# CC Model
## data
nbus    = length(opfdata.buses);
nload   = sum(opfdata.buses.bustype.==1);
buses = opfdata.buses

data = Dict()

# This assumes a small random perturbation
data[:Sigma_d]           = Matrix(Diagonal(ones(2nbus)))*(1/(nbus*nbus));

#data[:Sigma_d]           = Matrix(Diagonal(ones(2nbus)))*(1/(10*nbus));

data[:Va_min]            = -pi * ones(nbus)
data[:Va_max]            =  pi * ones(nbus)

## options
options = Dict()
#options[:lossless]       = false
#options[:current_rating] = false
#options[:epsilon_Vm]     = 0.01
#options[:epsilon_Va]     = 0.05
#options[:epsilon_Qg]     = 0.05
options[:gamma]           = 1.0
#options[:relax_Gamma]    = false
#options[:Gamma_type]     = Gamma_type
#options[:xtilde]         = xtilde
options[:print_level]     = 5
# these represent 1 - epsilon
options[:vareps_Va]       = 0.9 # 0.95
options[:vareps_Vm]       = 0.9 # 0.95
options[:vareps_Qg]       = 0.9 # 0.95

options[:jaccols]         = 2*nload #2*nload;
#options[:ccpar_Va]       = 0.00; # 0.95


# solve and output iterative cc model
OPF.cc_acopf_modelC_v1(opfdata,options,data)
