######## CC-AC-OPF (Chance-Constrained AC-Optimal Power Flow) ##################
# Formulation of probability constrained optimal power flow (OPF) problems.
#
# test_cc_acopf_modelC_v1.jl tests the implementation of modelC_v1, an iterative
# approach to the cc problem

###############################################################################
# 06/11/19, E.G.

# added more cases
# 06/17/19 E.G.

# 02/24/20, J.B., timing tests.


const path = pwd() * "/cases/"
#const case = "case30"
# case9 case30 case118 case300 case1354pegase case2383wp case2869pegase case9241pegase (1 refbus)
# case2736sp case2737sop  case3012wp case3120sp ( > 1 refbus)
# does not converge: case3375wp

## -----------------------------------------------------------------------------
## environment
## -----------------------------------------------------------------------------

import Pkg
Pkg.activate(dirname(dirname(dirname(path))))
Pkg.instantiate()

using Test
using MPCCases, Printf, MAT
using JuMP, JuMPUtil, Ipopt, MathProgBase
using SparseArrays, LinearAlgebra, Distributions
using DelimitedFiles
#using OPF

include("../src/OPF.jl")
#include("../src/jacobian_modelB_v1.jl")
#include("../src/cc_acopf_modelB_v1.jl")

# List with cases

#testCases = ["case9", "case30"];

# Run case9 twice to discard a "setup" run
testCases = ["case9","case9", "case30"];
#                "case1354pegase", "case2383wp", "case2869pegase", "case9241pegase"];
# testCases = ["case9","case9", "case30", "case118", "case300",
#                 "case1354pegase", "case2383wp", "case2869pegase", "case9241pegase"];
nCases = length(testCases);

# Data containters
times   = Array{Float64}(undef,nCases);
objs    = Array{Float64}(undef,nCases);
#its     = Array{Int64}(undef,nCases);
#convs   = Array{Bool}(undef,nCases);

for c in 1:nCases

        case = testCases[c];

        opfdata     = load_case(case, path, other=false);

        # CC Model
        ## data
        nbus    = length(opfdata.buses);
        nload   = sum(opfdata.buses.bustype.==1);
        buses = opfdata.buses

        data = Dict()

        # This assumes a small random perturbation
        # (1/(nbus*nbus))
        data[:Sigma_d]           = Matrix(Diagonal(ones(2nbus))*(1/(nbus*nbus)));

        #data[:Sigma_d]           = Matrix(Diagonal(ones(2nbus)))*(1/(10*nbus));

        data[:Va_min]            = -pi * ones(nbus)
        data[:Va_max]            =  pi * ones(nbus)

        ## options
        options = Dict()
        #options[:lossless]       = false
        #options[:current_rating] = false
        #options[:epsilon_Vm]     = 0.01
        #options[:epsilon_Va]     = 0.05
        #options[:epsilon_Qg]     = 0.05
        options[:gamma]           = 1.0
        #options[:relax_Gamma]    = false
        #options[:Gamma_type]     = Gamma_type
        #options[:xtilde]         = xtilde
        options[:print_level]     = 4;
        # these represent 1 - epsilon
        options[:vareps_Va]       = 0.9 # 0.95
        options[:vareps_Vm]       = 0.9 # 0.95
        options[:vareps_Qg]       = 0.9 # 0.95

        options[:maxIter]         = 50;

        options[:jaccols]         = 2*nload #2*nload;
        #options[:ccpar_Va]       = 0.00; # 0.95

        println("Running test: ",case);
        # solve and output iterative cc model

        (cm, topf1, bytes, gctime, memallocs)  = @timed OPF.cc_acopf_modelB_v2(opfdata,options,data);
        (cms, topf2, bytes, gctime, memallocs) = @timed OPF.cc_acopf_modelB_solve(cm,opfdata,options);

        #(vals, topf, bytes, gctime, memallocs) = @timed OPF.cc_acopf_modelC_EXIII(opfdata,options,data);
        #(topf,allopf,byopf,ccobj,acobj,iter,conv) = @time OPF.cc_acopf_modelC_EXIII(opfdata,options,data)

        println("Finished test in (s): ", topf1+topf2);

        times[c]   = (topf1+topf2);
        objs[c,1]  = getobjectivevalue(cms.m);
        # objs[c,2]  = vals[2];
        # its[c]     = vals[3];
        # convs[c]   = vals[4];

end

# Writing to files
open("data_EX3b_times.txt", "w") do io
    writedlm(io,times);
end

open("data_EX3b_objs.txt", "w") do io
    writedlm(io,objs);
end
