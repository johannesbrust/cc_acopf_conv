######## CC-AC-OPF (Chance-Constrained AC-Optimal Power Flow) ##################
# Formulation of probability constrained optimal power flow (OPF) problems.
#
# test_s_acopf.jl is intended to run larger test cases on the s_acopf model.
###############################################################################
# 06/18/19, J.B.


const path = pwd() * "/cases/"
const case = "case300" #case9 case30 case118

## -----------------------------------------------------------------------------
## environment
## -----------------------------------------------------------------------------
import Pkg
Pkg.activate(dirname(dirname(dirname(path))))
Pkg.instantiate()
using Test
using MPCCases, Printf, MAT
using JuMP, JuMPUtil, Ipopt, MathProgBase
using SparseArrays, LinearAlgebra, Distributions
#using OPF

include("../src/OPF.jl")
#include("../src/jacobian_modelB_v1.jl")
#include("../src/cc_acopf_modelB_v1.jl")

opfdata     = load_case(case, path, other=false);

# CC Model
## data
nbus    = length(opfdata.buses);
nload   = sum(opfdata.buses.bustype.==1);

# Classical/Original model
#mo  = OPF.s_acopf_model(opfdata);
mo  = OPF.acopf_model(opfdata);
mo  = OPF.acopf_solve(mo,opfdata);

OPF.acopf_outputAll(mo.m,:D,opfdata)


#Qgmax = cm.m[:cc_Qg_max];

# VM  = getvalue(getindex(cm.m,:Vm))
# VA  = getvalue(getindex(cm.m,:Va))
# QG  = getvalue(getindex(cm.m,:Qg))

# Constraint data

# GMm2 = GMm.*GMm;
# GMrl = sum(GMm2,dims=2);
#
# Z    = Normal(0,1);
# q    = quantile(Z,options[:vareps_Qg]);
