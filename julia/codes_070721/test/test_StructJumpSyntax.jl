######## CC-AC-OPF (Chance-Constrained AC-Optimal Power Flow) ##################
# Formulation of probability constrained optimal power flow (OPF) problems.
#
# test_StructJumpSyntax.jl is meant as a script to test StructJump functionality.
###############################################################################
# 05/13/19, J.B.


const path = pwd() * "/cases/"
const case = "case118" #case9 case30

## -----------------------------------------------------------------------------
## environment
## -----------------------------------------------------------------------------
import Pkg
Pkg.activate(dirname(dirname(dirname(path))))
Pkg.instantiate()
using MPCCases, Printf, MAT
using JuMP, JuMPUtil, Ipopt, MathProgBase
using SparseArrays, LinearAlgebra, Distributions
#using OPF

include("../src/OPF.jl")
#include("../src/jacobian_modelB_v1.jl")
#include("../src/cc_acopf_modelB_v1.jl")

opfdata     = load_case(case, path, other=false);

# CC Model
## data
nbus    = length(opfdata.buses);
nload   = sum(opfdata.buses.bustype.==1);
busidx  = 1:nbus;

refidx  = opfdata.bus_ref;

genidx      = busidx[opfdata.buses.bustype.==2];

BusGeners   = opfdata.BusGenerators;

# Assume a map: Bus idx -> Generator idx
gidx = BusGeners[genidx];

ridx = BusGeners[refidx]
#maximum(gidx) <= length(genidx)
