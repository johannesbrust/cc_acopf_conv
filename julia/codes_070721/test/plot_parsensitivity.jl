#------------- LD-AC-OPF (Load Dynamic AC-Optimal Power Flow) -----------------#
# plot_parsensitivity.jl is a script to plot outcomes of the parameter sensitivity
# computations.
#
# Script requires data files.
#------------------------------------------------------------------------------#
# 09/04/19, J.B.
# 09/05/19, J.B., modification to plot norm(Vm/V0)

import Pkg
Pkg.activate();

using Plots;
using DelimitedFiles;

# Flag to decide on which plot.
whichPlot = 1; # 1: Objective, 2: Norms

# Read input data
lddata  = readdlm("data_ld.txt",'\t',Float64,'\n');
rdata   = readdlm("data_rld.txt",'\t',Int,'\n'); # Columns beta|gamma|mu
eps     = readdlm("data_eps.txt",'\t',Float64,'\n');

n       = length(eps);
on      = ones(n,1);

# Plotting. lddata's columns: beta|gamma|mu|alpha|obj
(mxobj,mxidx) = findmax(lddata[:,5]);
(zobj,zidx)   = (lddata[1,5],1);

znV           = lddata[1,6];
mxnV          = lddata[mxidx,6];

mxr           = lddata[mxidx,:];

bobj          = lddata[rdata[:,1],5];
gobj          = lddata[rdata[:,2],5];
mobj          = lddata[rdata[:,3],5];

bnV           = lddata[rdata[:,1],6];
gnV           = lddata[rdata[:,2],6];
mnV           = lddata[rdata[:,3],6];

#objs        = [zobj.*on,bobj,gobj,mobj,mxobj.*on];

objs        = [bobj,gobj,mobj];
nVs         = [bnV,gnV,mnV];

# ldlables    = ["\$ \\textnormal{LD-1:}(\\beta=0,\\gamma=0,\\mu=0)\$",
#                 "B",
#                 "C",
#                 "D",
#                 "LD-MAX:max(Objective)"]

ldlables    = [ "\$ \\textnormal{LD-1:}(\\beta=\\epsilon,\\gamma=0,\\mu_P=0)\$", #\\epsilon
                "\$ \\textnormal{LD-2:}(\\beta=0,\\gamma= \\epsilon,\\mu_P=0)\$",
                "\$ \\textnormal{LD-3:}(\\beta=0,\\gamma=0,\\mu_P= \\epsilon)\$"];


ltypes  = :solid; #[:dash, :solid, :solid, :solid, :dash];

labelx  = "\$\\epsilon\$"
ldtitle = "Load-Dynamic Models (LD-M)"

mxlabel = "\$ \\textnormal{LD-MAX:}("*" \\beta="*string(mxr[1])*",\\gamma="*string(mxr[2])*",\\mu_P="*string(Int(mxr[3]))*")\$"

# Plots for objective value
px1 = 0; plw1 = 0; px2 = 0; px3 = 0; plabely = "";

if whichPlot == 1

  px1   = zobj.*on;
  plw1  = 2;

  px2   = objs;
  px3   = mxobj.*on;
  plabely = "Objective";

end

if whichPlot == 2

  px1   = znV.*on;
  plw1  = 5;

  px2   = nVs;
  px3   = mxnV.*on;
  plabely = "\$ \\textnormal{norm}(V^*/V_0)\$";

end

plot(eps, px1,label = "\$ \\textnormal{LD-0:}(\\beta=0,\\gamma=0,\\mu_P=0)\$", lw = plw1, linestyle = :dot)
plot!(eps,px2,label = ldlables, lw = 3, linestyles = ltypes, marker = (:circle,2))
plot!(eps,px3,label = mxlabel,  lw = 2,  linestyle = :dot)

title!(ldtitle);
xlabel!(labelx);
ylabel!(plabely);
