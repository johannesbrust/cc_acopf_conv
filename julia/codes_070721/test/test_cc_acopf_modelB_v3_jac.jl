################## PCD (Probability Constrained Dispatch) ################
# Formulation of probability constrained optimal power flow (OPF) problems.
#
# test_cc_acopf_modelB_v3.jl is a script for the implementation of modelB_v3.
# This test is intended to compare the Jacobian calculations.
###########################################################################
# 06/20/19, J.B.

const path = pwd() * "/cases/"
const case = "case30" #case9 case30

## -----------------------------------------------------------------------------
## environment
## -----------------------------------------------------------------------------
import Pkg
Pkg.activate(dirname(dirname(dirname(path))))
Pkg.instantiate()
using Test
using MPCCases, Printf, MAT
using JuMP, JuMPUtil, Ipopt, MathProgBase
using SparseArrays, LinearAlgebra, Distributions
#using OPF

include("../src/OPF.jl")

# LU factorization and solves
include("../src/lufac.jl")
include("../src/solve_lufac.jl")

#include("../src/cc_acopf_modelB_v1.jl")

opfdata     = load_case(case, path, other=false);

# CC Model
## data
nbus = length(opfdata.buses);

data = Dict()
data[:Sigma_d]           = Matrix(Diagonal(ones(2nbus)))
data[:Va_min]            = -pi * ones(nbus)
data[:Va_max]            =  pi * ones(nbus)

## options
options = Dict()
#options[:lossless]       = false
#options[:current_rating] = false
#options[:epsilon_Vm]     = 0.01
options[:epsilon_Va]     = 0.05
#options[:epsilon_Qg]     = 0.05
options[:gamma]          = 1.0
#options[:relax_Gamma]    = false
#options[:Gamma_type]     = Gamma_type
#options[:xtilde]         = xtilde
options[:print_level]    = 5

# Model initalization
cm 		                   = OPF.cc_acopf_modelB_v3(opfdata,options,data)
#cm 		                 = OPF.acopf_solve(cm,opfdata)

classm                      = OPF.s_acopf_model(opfdata);
classm                      = OPF.acopf_solve(classm,opfdata)

# Retrieving values from the classical model.
VM                        = getvalue(getindex(classm.m,:Vm))
VA                        = getvalue(getindex(classm.m,:Va))
PG                        = getvalue(getindex(classm.m,:Pg))
QG                        = getvalue(getindex(classm.m,:Qg))
PD                        = getvalue(getindex(classm.m,:Pd))
QD                        = getvalue(getindex(classm.m,:Qd))

# Setting values in the chance constrained model
setvalue(getindex(cm.m,:Vm),VM);
setvalue(getindex(cm.m,:Va),VA);
setvalue(getindex(cm.m,:Pg),PG);
setvalue(getindex(cm.m,:Qg),QG);
setvalue(getindex(cm.m,:Pd),PD);
setvalue(getindex(cm.m,:Qd),QD);

# Analytic Gamma

jac_a                           = OPF.jacobian_modelB_v1(opfdata,VM,VA);
refIdx, genIdx, ldIdx, nrefIdx  = OPF.RGL_idx_mb(opfdata);

#nrefgenIdx  = genIdx[genIdx .!= refIdx];

nbus 							= length(opfdata.buses);
ngen 							= length(opfdata.generators);

genidxs                         = 1:ngen;
genref                          = opfdata.BusGenerators[refIdx];
genidxsNref                     = genidxs[genidxs.!=genref];

rows 							= vcat(nrefIdx,nbus .+ nrefIdx);
#cols 							= vcat(ngen .+ genIdx, (2*ngen .+ ldIdx), (2*ngen + nbus .+ nrefIdx)); # Assume reference at index 1
cols 							= vcat((ngen .+ genidxsNref), (2*ngen .+ ldIdx), (2*ngen + nbus .+ nrefIdx));
colsp 							= vcat(2*(ngen+nbus) .+ ldIdx, 2*(ngen+nbus) + nbus .+ ldIdx );

dFdx                            = jac_a[rows,cols];
dFdp                            = jac_a[rows,colsp];

# Permuted columns of dFdx
sVa = nbus;
eVa = 2*(nbus-1);
iVa = Vector(sVa:eVa);

sdQ = 1;
edQ = length(genidxsNref);
iQ  = Vector(sdQ:edQ);

sVm = length(genidxsNref)+1;
eVm = sVm+length(ldIdx)-1;
iVm = Vector(sVm:eVm);

rowsp = Vector(1:2*(nbus-1));

dFdxP                           = [dFdx[rowsp,iVa] dFdx[rowsp,iQ] dFdx[rowsp,iVm]];

# LU factorization of Jacobian
Lc,Uc                           = lufac(dFdxP);

# Gamma computations
GMd                             = - dFdx \ dFdp;
GMlu                            = solve_lufac(Lc,Uc,-dFdp);

colsGM                          = Vector(1:size(dFdp,2));

GMluP                           = [GMlu[iVa,colsGM]; GMlu[iQ,colsGM]; GMlu[iVm,colsGM]];

#GMV3        = getvalue(getindex(cm.m,:GM));

if size(GMd) == size(GMluP)

  err = GMd - GMluP;

end


# nvars   = 2*ngen+4*nbus
# ncons   = 2*nbus
#
# # Jump model and computation of Jacobian
# # Uses MathProgBase
# jmdl    = sm.m
# d       = JuMP.NLPEvaluator(jmdl)
#
# MathProgBase.initialize(d,[:Jac])
#
# # Variables
# # Pg, Qg, Vm, Va, Pd, Qd
# xopt              = zeros(nvars)
# PG                = getvalue(getindex(jmdl,:Pg))
# QG                = getvalue(getindex(jmdl,:Qg))
# VM                = getvalue(getindex(jmdl,:Vm))
# VA                = getvalue(getindex(jmdl,:Va))
# PD                = getvalue(getindex(jmdl,:Pd))
# QD                = getvalue(getindex(jmdl,:Qd))
#
# idxs              = 1
# idxe              = ngen
# xopt[idxs:idxe]   = PG; idxs = idxe + 1; idxe = idxs + ngen - 1
# xopt[idxs:idxe]   = QG; idxs = idxe + 1; idxe = idxs + nbus - 1
# xopt[idxs:idxe]   = VM; idxs = idxe + 1; idxe = idxs + nbus - 1
# xopt[idxs:idxe]   = VA; idxs = idxe + 1; idxe = idxs + nbus - 1
# xopt[idxs:idxe]   = PD; idxs = idxe + 1; idxe = idxs + nbus - 1
# xopt[idxs:idxe]   = QD
#
# # Indexing
# busidx 	= 1:nbus;
# ldidx 	= busidx[buses.bustype.==1];
# gidx 	= busidx[buses.bustype.==2];
# refidx 	= opfdata.bus_ref;
# nrefidx = busidx[buses.bustype.!=3];
#
# o2nb 	= ones(2*nbus,1);
# I2nb 	= diagm(0 => o2nb[:]);
#
# 	#rhs1_ 	= I2nb[nrefidx,ldidx];
# rhs1_ 	= I2nb[1:nbus,ldidx];
# 	#rhs2_ 	= I2nb[ngen.+nrefidx,ngen.+ldidx];
# rhs2_ 	= I2nb[(nbus+1):(2*nbus),nbus.+ldidx];
#
# 	#zl 		= zeros(length(nrefidx),nload);
# nload 	= length(ldidx);
# zl 		= zeros(nbus,nload);
#
# rhs1 	= cat(rhs1_,zl,dims=2);
# rhs2 	= cat(zl,rhs2_,dims=2);
#
# dFdQ 	= I2nb[(nbus+1):2*nbus,nbus.+gidx];



# Jacobian computations
#full_jac          = zeros(2*ncons,nvars)
# (Idxi,Idxj)       = MathProgBase.jac_structure(d)
# jac_v             = zeros(length(Idxi))
#
# MathProgBase.eval_jac_g(d,jac_v,xopt)
#
# sjac              = sparse(Idxi,Idxj,jac_v,2*ncons,nvars) # sparse Jacobian
# jac_mpb           = Array(sjac) # dense Jacobian
#
# # Analytic Jacobian
# jac_a             = jac_analytic(opfdata,PG,QG,VM,VA,PD,QD);
#
# # Analytic Jacobian (sum-if implementation)
# jac_a_sif         = jac_analytic_sumif(opfdata,PG,QG,VM,VA,PD,QD);
#
# # Column extractions Vm, Va
#
# colscmp           = (2*ngen+1):(2*(ngen+nbus));
# cols_mpb          = jac_mpb[1:ncons,colscmp];
#
# cols_a            = jac_a[:,colscmp];
#
# err               = cols_mpb-cols_a;
#
# cols_asif         = jac_a_sif[:,colscmp];
#
# # Jacobian values
# #sjm               = sj.m;
# #jac_v_p           = getvalue(getindex(sjm,:jacp));
#
# jmdl    = sj.m
# d_      = JuMP.NLPEvaluator(jmdl)
#
# MathProgBase.initialize(d_,[:Jac])
#
# g               = zeros(2*ncons+4*nbus*nbus,1);
#
# MathProgBase.eval_g(d_,g,xopt);
#
# jacVmP              = zeros(nbus,nbus);
# idxs                =0;
# idxe                =0;
#
# jacVaP              = zeros(nbus,nbus);
# idxsa               =0;
# idxea               =0;
#
# jacVmQ              = zeros(nbus,nbus);
# idxsmq              =0;
# idxemq              =0;
#
# jacVaQ              = zeros(nbus,nbus);
# idxsaq              =0;
# idxeaq              =0;
#
# for k=1:nbus
#
#     idxs = 2*ncons +(k-1)*nbus + 1;
#     idxe = 2*ncons +k*nbus;
#
#     #jacJP[:,k] = g[idxs:idxe,1];
#     jacVmP[k,:] = g[idxs:idxe,1];
#
#     idxsa = 2*ncons + nbus*nbus +(k-1)*nbus + 1;
#     idxea = 2*ncons + nbus*nbus + k*nbus;
#
#     #jacJP[:,k] = g[idxs:idxe,1];
#     jacVaP[k,:] = g[idxsa:idxea,1];
#
#     idxsmq = 2*(ncons + nbus*nbus) + (k-1)*nbus + 1;
#     idxemq = 2*(ncons + nbus*nbus) + k*nbus;
#
#     #jacJP[:,k] = g[idxs:idxe,1];
#     jacVmQ[k,:] = g[idxsmq:idxemq,1];
#
#     idxsaq = 2*(ncons + nbus*nbus) + nbus*nbus + (k-1)*nbus + 1;
#     idxeaq = 2*(ncons + nbus*nbus) + nbus*nbus + k*nbus;
#
#     #jacJP[:,k] = g[idxs:idxe,1];
#     jacVaQ[k,:] = g[idxsaq:idxeaq,1];
#
# end
#
# colscmp1            = (2*ngen+1):(2*ngen+nbus);
# jac_a_cmp           = jac_a[1:nbus,colscmp1];
# jac_mpb_cmp         = jac_mpb[1:nbus,colscmp1];
#
# colscmp2            = (2*ngen+nbus+1):(2*(ngen+nbus));
# jac_a_cmp2          = jac_a[1:nbus,colscmp2];
# jac_mpb_cmp2        = jac_mpb[1:nbus,colscmp2];
#
# #colscmp1             = (2*ngen+1):(2*ngen+nbus);
# jac_a_cmpq           = jac_a[(nbus+1):2*nbus,colscmp1];
# jac_mpb_cmpq         = jac_mpb[(nbus+1):2*nbus,colscmp1];
#
# #colscmp2             = (2*ngen+nbus+1):(2*(ngen+nbus));
# jac_a_cmp2q          = jac_a[(nbus+1):2*nbus,colscmp2];
# jac_mpb_cmp2q        = jac_mpb[(nbus+1):2*nbus,colscmp2];

#obj_    = MathProgBase.eval_f(d_,xopt)

#MathProgBase.initialize(d,[:Jac])


# Calling test

#Pg = ones(ngen,1);
#Qg = ones(ngen,1);
#Vm = ones(nbus,1);
#Va = ones(nbus,1);
#Pd = ones(nbus,1);
#Qd = ones(nbus,1);

# Debugging Juno.@enter
#jac_analytic(opfdata,Pg,Qg,Vm,Va,Pd,Qd)
