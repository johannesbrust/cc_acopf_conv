############## LD-AC-OPF (Load Dynamic AC-Optimal Power Flow) ##################
# Formulation of load dynamic optimal power flow (OPF) problems.
#
# test_ld_acopf.jl is intended for initial tests of a LD-AC-OPF model.
################################################################################
# 09/03/19, J.B.

const path = pwd() * "/cases/"
const case = "case118" #case9 case30 case118

## -----------------------------------------------------------------------------
## environment
## -----------------------------------------------------------------------------
import Pkg
Pkg.activate(dirname(dirname(dirname(path))))
Pkg.instantiate()
using Test
using MPCCases, Printf, MAT
using JuMP, JuMPUtil, Ipopt, MathProgBase
using SparseArrays, LinearAlgebra, Distributions
#using OPF

include("../src/OPF.jl")
#include("../src/jacobian_modelB_v1.jl")
#include("../src/cc_acopf_modelB_v1.jl")

opfdata     = load_case(case, path, other=false);

nbus    = length(opfdata.buses);
nload   = sum(opfdata.buses.bustype.==1);

# Classical/Original model
#mo  = OPF.s_acopf_model(opfdata);
mo  = OPF.acopf_model(opfdata);
mo  = OPF.acopf_solve(mo,opfdata);

println("Objective value: ", getobjectivevalue(mo.m), "USD/hr");
#OPF.acopf_outputAll(mo.m,:D,opfdata);

# Load dynamic model.
# Expect the same output as for Classical model.
ld_m = OPF.ld_acopf_model(opfdata);
ld_m = OPF.acopf_solve(ld_m,opfdata);

println("Objective value: ", getobjectivevalue(ld_m.m), "USD/hr");
#OPF.acopf_outputAll(ld_m.m,:D,opfdata);

# Update dynamic model. Expect updated objective value
alpidx  = 1;
betidx  = 2;
gamidx  = 3;
muidx   = 4;

setvalue(JuMP.NonlinearParameter(ld_m.m,betidx),1e-3);
setvalue(JuMP.NonlinearParameter(ld_m.m,alpidx),(1-1e-3));
#setvalue(getindex(ld_m.m,:ld_beta),1e-3);
#setvalue(ld_beta,1e-3);
ld_m = OPF.acopf_solve(ld_m,opfdata);

println("Objective value: ", getobjectivevalue(ld_m.m), "USD/hr");
