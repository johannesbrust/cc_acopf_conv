######## CC-AC-OPF (Chance-Constrained AC-Optimal Power Flow) ##################
# Formulation of probability constrained optimal power flow (OPF) problems.
#
# test_cc_acopf_modelB_v2.jl tests the implementation of V2 (Version-2) of the
# chance-constrained AC-OPF model.
###############################################################################
# 05/02/19, J.B.


const path = pwd() * "/cases/"
const case = "case30" #case9 case30 case118

## -----------------------------------------------------------------------------
## environment
## -----------------------------------------------------------------------------
import Pkg
Pkg.activate(dirname(dirname(dirname(path))))
Pkg.instantiate()
using Test
using MPCCases, Printf, MAT
using JuMP, JuMPUtil, Ipopt, MathProgBase
using SparseArrays, LinearAlgebra, Distributions
#using OPF

include("../src/OPF.jl")
#include("../src/jacobian_modelB_v1.jl")
#include("../src/cc_acopf_modelB_v1.jl")

opfdata     = load_case(case, path, other=false);

# CC Model
## data
nbus    = length(opfdata.buses);
nload   = sum(opfdata.buses.bustype.==1);

data = Dict()
# This assumes a small random perturbation
data[:Sigma_d]           = Matrix(Diagonal(ones(2nbus)))*(1/(nbus*nbus));

#data[:Sigma_d]           = Matrix(Diagonal(ones(2nbus)))*(1/(10*nbus));

data[:Va_min]            = -pi * ones(nbus)
data[:Va_max]            =  pi * ones(nbus)

## options
options = Dict()
#options[:lossless]       = false
#options[:current_rating] = false
#options[:epsilon_Vm]     = 0.01
#options[:epsilon_Va]     = 0.05
#options[:epsilon_Qg]     = 0.05
options[:gamma]           = 1.0
#options[:relax_Gamma]    = false
#options[:Gamma_type]     = Gamma_type
#options[:xtilde]         = xtilde
options[:print_level]     = 5
options[:vareps_Va]       = 0.9 # 0.95
options[:vareps_Vm]       = 0.9 # 0.95
options[:vareps_Qg]       = 0.9 # 0.95
options[:jaccols]         = 2*nload #2*nload;
#options[:ccpar_Va]       = 0.00; # 0.95

cm  = OPF.cc_acopf_modelB_v2(opfdata,options,data)

# Solve w/o initial point
#cm  = OPF.acopf_solve(cm,opfdata);

# Solve from ACOPF initial point
cms = OPF.cc_acopf_modelB_solve(cm,opfdata,options);

OPF.acopf_outputAll(cm.m,:D,opfdata)

# Classical/Original model
mo  = OPF.s_acopf_model(opfdata,options);
mo  = OPF.acopf_solve(mo,opfdata);

OPF.acopf_outputAll(mo.m,:D,opfdata)


#Qgmax = cm.m[:cc_Qg_max];

# VM  = getvalue(getindex(cm.m,:Vm))
# VA  = getvalue(getindex(cm.m,:Va))
# QG  = getvalue(getindex(cm.m,:Qg))

# Constraint data

# GMm2 = GMm.*GMm;
# GMrl = sum(GMm2,dims=2);
#
# Z    = Normal(0,1);
# q    = quantile(Z,options[:vareps_Qg]);
