############## CC-AC-OPF (Chance-Constrained AC-Optimal Power Flow) ############
# Formulation of probability constrained optimal power flow (OPF) problems.
#
# cc_acopf_modelB_v3.jl is an initial implementation to compute a JuMP model that uses
# equality constraints to represent sensitivities. This method is based on
# cc_acopf_modelB_v2.jl and computes the sensitivies defined by
#
# [partial F / partial y] [dy / dx] = -[partial F / partial x],
#
# via a LU factorization, and triangular solves. Here F represents the power-flow
# equations and y = [Qg^L Vm^L Va], x = [Pd Qd].
# Superscript '^L' refers to load buses.
# The sensitivities are computed in Gamma := [dy / dx], via the factorization
# and linear solves:
#
# [partial F / partial y] P 	= L U,
# [dy / dx] 					= - P (U \ ( L \ [partial F / partial x] )),
#
# where P is a permutation matrix.
################################################################################
# 04/24/19, J.B.
# 05/02/19, J.B., Implementation of Vm, Qg chance constraints
# 05/13/19, J.B., Modification of chance constraints on generators to account for
# non-unit reference index.
# 06/19/19, J.B., Implementation of V3, which uses the LU factorization and
# triangular solves to compute Gamma sensitivites.

function cc_acopf_modelB_v3(opf_data, options::Dict=Dict(), data::Dict=Dict()) # , options::Dict=Dict(), data::Dict=Dict()

	# shortcuts for compactness
	lines = opf_data.lines; buses = opf_data.buses; generators = opf_data.generators; baseMVA = opf_data.baseMVA
	busIdx = opf_data.BusIdx; FromLines = opf_data.FromLines; ToLines = opf_data.ToLines; BusGeners = opf_data.BusGenerators;

	nbus = length(buses); nline = length(lines); ngen = length(generators); nload = length(findall(buses.bustype .== 1))


	## parse options

	#lossless       = haskey(options, :lossless)       ? options[:lossless]       : false
	#current_rating = haskey(options, :current_rating) ? options[:current_rating] : false
	#epsilon_Vm     = haskey(options, :epsilon_Vm)     ? options[:epsilon_Vm]     : 0.05

	#epsilon_Qg     = haskey(options, :epsilon_Qg)     ? options[:epsilon_Qg]     : 0.05

	#relax_Gamma    = haskey(options, :relax_Gamma)    ? options[:relax_Gamma]    : false

	#xtilde         = haskey(options, :xtilde)         ? options[:xtilde]         : true
	#Gamma_type     = haskey(options, :Gamma_type)     ? options[:Gamma_type]     : :d

	epsilon_Va  = haskey(options, :epsilon_Va)   ? options[:epsilon_Va]   : 0.05
	gamma       = haskey(options, :gamma)        ? options[:gamma]        : 1.0
	print_level = haskey(options, :print_level)  ? options[:print_level]  : 0

	vareps_Va 	= haskey(options, :vareps_Va)    ? options[:vareps_Va]    : 0.9
	vareps_Vm 	= haskey(options, :vareps_Vm)    ? options[:vareps_Vm]    : 0.9
	vareps_Qg 	= haskey(options, :vareps_Qg)    ? options[:vareps_Qg]    : 0.9

	ccpar_Va 	= haskey(options, :ccpar_Va)     ? options[:ccpar_Va]     : 1.0

	jaccols 	= haskey(options, :jaccols)      ? options[:jaccols]      : 2*nload

	Sig_d    	= haskey(data, :Sigma_d) 		 ? data[:Sigma_d] 		  : Matrix(Diagonal(ones(2nbus)))
	Va_min 		= haskey(data, :Va_min)  		 ? data[:Va_min]  		  : -pi * ones(nbus)
	Va_max 		= haskey(data, :Va_max)  		 ? data[:Va_max]  		  :  pi * ones(nbus)
	Z 			= Normal(0,1)

	# if lossless && !current_rating
	#   println("warning: lossless assumption requires `current_rating` instead of `power_rating`\n")
	#   current_rating = true
	# end


	# @assert(nload + ngen == nbus); NOT assert bc some buses can have more than one generator...

	busidx 		= 1:nbus; #ldidx = Array{Int}(undef,nload);
	ldidx 		= busidx[buses.bustype.==1];
	gidx 		= busidx[buses.bustype.==2];
	refidx 		= opf_data.bus_ref;
	nrefidx 	= busidx[buses.bustype.!=3];
	nldidx 		= busidx[buses.bustype.!=1];

	# branch admitances
	YffR,YffI,YttR,YttI,YftR,YftI,YtfR,YtfI,YshR,YshI = computeAdmitances(lines, buses, baseMVA)

	#
	# model
	#
	if "19" ∈ split(string(Pkg.installed()["JuMP"]), ".")
		opfmodel = Model(with_optimizer(Ipopt.Optimizer))
	else
		opfmodel = Model(solver=IpoptSolver(print_level=print_level))
	end

	@variable(opfmodel, generators[i].Pmin <= Pg[i=1:ngen] <= generators[i].Pmax)

	# 05/02/19, J.B., Modification of bound constraints as chance constraints are included
	#@variable(opfmodel, generators[i].Qmin <= Qg[i=1:ngen] <= generators[i].Qmax)
	@variable(opfmodel, Qg[i=1:ngen])
	setlowerbound(Qg[BusGeners[refidx][1]],generators[BusGeners[refidx][1]].Qmin);
	setupperbound(Qg[BusGeners[refidx][1]],generators[BusGeners[refidx][1]].Qmax);

	#@variable(opfmodel, buses[i].Vmin <= Vm[i=1:nbus] <= buses[i].Vmax)
	#ngenm1 = length(gidx);
	@variable(opfmodel, Vm[i=1:nbus])
	setlowerbound(Vm[refidx],buses[refidx].Vmin);
	setupperbound(Vm[refidx],buses[refidx].Vmax);

	for i in 1:length(gidx)

		setlowerbound(Vm[gidx[i]],buses[gidx[i]].Vmin);
		setupperbound(Vm[gidx[i]],buses[gidx[i]].Vmax);

	end

	#@variable(opfmodel, Va_min[i]		<= Va[i=1:nbus] <= Va_max[i])
	@variable(opfmodel, Va[1:nbus])

	## assumes no buses have generator and load
	@variable(opfmodel, buses[i].Pd/baseMVA <= Pd[i=1:nbus] <= buses[i].Pd/baseMVA)
	@variable(opfmodel, buses[i].Qd/baseMVA <= Qd[i=1:nbus] <= buses[i].Qd/baseMVA)

	# Additional (array) variable that represents sensitivities.
	# GM := [dy / dx]
	#@variable(opfmodel, GM[1:2*(nbus-1),1:jaccols]);

	#fix the voltage angle at the reference bus
	if "19" ∈ split(string(Pkg.installed()["JuMP"]), ".")
		set_lower_bound(Va[opf_data.bus_ref], buses[opf_data.bus_ref].Va)
		set_upper_bound(Va[opf_data.bus_ref], buses[opf_data.bus_ref].Va)
	else
		setlowerbound(Va[opf_data.bus_ref], buses[opf_data.bus_ref].Va)
		setupperbound(Va[opf_data.bus_ref], buses[opf_data.bus_ref].Va)

		# Fix
		#fix(Va[opf_data.bus_ref], buses[opf_data.bus_ref].Va);
	end

	@NLobjective(opfmodel, Min, sum( generators[i].coeff[generators[i].n-2]*(baseMVA*Pg[i])^2
	+generators[i].coeff[generators[i].n-1]*(baseMVA*Pg[i])
	+generators[i].coeff[generators[i].n  ] for i=1:ngen))

	#
	# power flow balance
	#
	#real part
	@NLconstraint(opfmodel, P[b=1:nbus],
	( sum( YffR[l] for l in FromLines[b]) + sum( YttR[l] for l in ToLines[b]) + YshR[b] ) * Vm[b]^2
	+ sum( Vm[b]*Vm[busIdx[lines[l].to]]  *( YftR[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  ) + YftI[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )) for l in FromLines[b] )
	+ sum( Vm[b]*Vm[busIdx[lines[l].from]]*( YtfR[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) + YtfI[l]*sin(Va[b]-Va[busIdx[lines[l].from]])) for l in ToLines[b]   )
	- ( sum(baseMVA*Pg[g] for g in BusGeners[b]) - sum(baseMVA*Pd[l] for l in busIdx[b]) ) / baseMVA      # Sbus part
	==0)
	#imaginary part
	@NLconstraint(opfmodel, Q[b=1:nbus],
	( sum(-YffI[l] for l in FromLines[b]) + sum(-YttI[l] for l in ToLines[b]) - YshI[b] ) * Vm[b]^2
	+ sum( Vm[b]*Vm[busIdx[lines[l].to]]  *(-YftI[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  ) + YftR[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )) for l in FromLines[b] )
	+ sum( Vm[b]*Vm[busIdx[lines[l].from]]*(-YtfI[l]*cos(Va[b]-Va[busIdx[lines[l].from]]) + YtfR[l]*sin(Va[b]-Va[busIdx[lines[l].from]])) for l in ToLines[b]   )
	- ( sum(baseMVA*Qg[g] for g in BusGeners[b]) - sum(baseMVA*Qd[l] for l in busIdx[b]) ) / baseMVA      #Sbus part
	==0)
	#
	# branch/lines flow limits
	#
	# 04/01/19, J.B., Comment function calls
	@constraintref F_fr[1:nline]  ## from bus, TODO: won't work in JuMP v0.19
	@constraintref F_to[1:nline]  ## to bus, TODO: won't work in JuMP v0.19
	nlinelim=0
	for l in 1:nline
		if lines[l].rateA!=0 && lines[l].rateA<1.0e10
			nlinelim += 1
			flowmax=(lines[l].rateA/baseMVA)^2

			#branch apparent power limits (from bus)
			Yff_abs2=YffR[l]^2+YffI[l]^2; Yft_abs2=YftR[l]^2+YftI[l]^2
			Yre=YffR[l]*YftR[l]+YffI[l]*YftI[l]; Yim=-YffR[l]*YftI[l]+YffI[l]*YftR[l]
			F_fr[l] = @NLconstraint(opfmodel,
			Vm[busIdx[lines[l].from]]^2 *
			( Yff_abs2*Vm[busIdx[lines[l].from]]^2 + Yft_abs2*Vm[busIdx[lines[l].to]]^2
			+ 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]*(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]]))
			)
			- flowmax <=0)

			#branch apparent power limits (to bus)
			Ytf_abs2=YtfR[l]^2+YtfI[l]^2; Ytt_abs2=YttR[l]^2+YttI[l]^2
			Yre=YtfR[l]*YttR[l]+YtfI[l]*YttI[l]; Yim=-YtfR[l]*YttI[l]+YtfI[l]*YttR[l]
			F_to[l] = @NLconstraint(opfmodel,
			Vm[busIdx[lines[l].to]]^2 *
			( Ytf_abs2*Vm[busIdx[lines[l].from]]^2 + Ytt_abs2*Vm[busIdx[lines[l].to]]^2
			+ 2*Vm[busIdx[lines[l].from]]*Vm[busIdx[lines[l].to]]*(Yre*cos(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]])-Yim*sin(Va[busIdx[lines[l].from]]-Va[busIdx[lines[l].to]]))
			)
			- flowmax <=0)
		end
	end
	JuMP.registercon(opfmodel, :F_fr, F_fr)
	JuMP.registercon(opfmodel, :F_to, F_to)

	## Jacobian columns w.r.t. Vm, Va
	# jac = 	| dFP/dVm dFP/dVa | = 	| jacVmP jacVaP |
	#			| dFQ/dVm dFQ/dVa | 	| jacVmQ jacVaQ |

	# Real parts
	# VmP (Voltage magnitudes)
	@NLexpression(opfmodel,jacVmP[b=1:nbus,j=1:nbus],
	sum( 2*(YshR[b] * Vm[b]) for l in 1:1 if b == j) +
	sum( 2*(YffR[l] * Vm[b]) + Vm[busIdx[lines[l].to]] * ( YftR[l]*cos(Va[b]-Va[busIdx[lines[l].to]] ) + YftI[l]*sin(Va[b]-Va[busIdx[lines[l].to]])) for l in FromLines[b] if b == j) +
	sum( Vm[b] * ( YftR[l]*cos(Va[b]-Va[busIdx[lines[l].to]] ) + YftI[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )) for l in FromLines[b] if busIdx[lines[l].to] == j ) +
	sum( 2*(YttR[l] * Vm[b]) + Vm[busIdx[lines[l].from]] * ( YtfR[l]*cos(Va[b]-Va[busIdx[lines[l].from]] ) + YtfI[l]*sin(Va[b]-Va[busIdx[lines[l].from]])) for l in ToLines[b] if b == j ) +
	sum( Vm[b] * ( YtfR[l]*cos(Va[b]-Va[busIdx[lines[l].from]] ) + YtfI[l]*sin(Va[b]-Va[busIdx[lines[l].from]]  )) for l in ToLines[b] if busIdx[lines[l].from] == j ))

	# VaP, Voltage angles for real parts
	@NLexpression(opfmodel,jacVaP[b=1:nbus,j=1:nbus],
	sum( Vm[b]*Vm[busIdx[lines[l].to]] * ( -YftR[l]*sin(Va[b]-Va[busIdx[lines[l].to]] ) + YftI[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  )) for l in FromLines[b] if b == j) +
	sum( Vm[b] * Vm[busIdx[lines[l].to]] * ( YftR[l]*sin(Va[b]-Va[busIdx[lines[l].to]] ) -YftI[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  )) for l in FromLines[b] if busIdx[lines[l].to] == j) +
	sum( Vm[b]*Vm[busIdx[lines[l].from]] * ( -YtfR[l]*sin(Va[b]-Va[busIdx[lines[l].from]] ) + YtfI[l]*cos(Va[b]-Va[busIdx[lines[l].from]]  )) for l in ToLines[b] if b == j ) +
	sum( Vm[b] * Vm[busIdx[lines[l].from]] * ( YtfR[l]*sin(Va[b]-Va[busIdx[lines[l].from]] ) -YtfI[l]*cos(Va[b]-Va[busIdx[lines[l].from]]  )) for l in ToLines[b] if busIdx[lines[l].from] == j) )

	# Imaginary parts
	# VmQ (Voltage magnitudes)
	@NLexpression(opfmodel,jacVmQ[b=1:nbus,j=1:nbus],
	sum( 2*(-YshI[b])*Vm[b] for l in 1:1 if b == j ) +
	sum( 2*(-YffI[l]*Vm[b]) + Vm[busIdx[lines[l].to]] * ( -YftI[l]*cos(Va[b]-Va[busIdx[lines[l].to]] ) + YftR[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )) for l in FromLines[b] if b == j) +
	sum( Vm[b] * ( -YftI[l]*cos(Va[b]-Va[busIdx[lines[l].to]] ) + YftR[l]*sin(Va[b]-Va[busIdx[lines[l].to]]  )) for l in FromLines[b] if busIdx[lines[l].to] == j ) +
	sum( 2*(-YttI[l])*Vm[b] + Vm[busIdx[lines[l].from]] * ( -YtfI[l]*cos(Va[b]-Va[busIdx[lines[l].from]] ) + YtfR[l]*sin(Va[b]-Va[busIdx[lines[l].from]]  )) for l in ToLines[b] if b == j) +
	sum( Vm[b] * ( -YtfI[l]*cos(Va[b]-Va[busIdx[lines[l].from]] ) + YtfR[l]*sin(Va[b]-Va[busIdx[lines[l].from]]  )) for l in ToLines[b] if busIdx[lines[l].from] == j ) )

	# VaQ
	@NLexpression(opfmodel,jacVaQ[b=1:nbus,j=1:nbus],
	sum( Vm[b]*Vm[busIdx[lines[l].to]] * ( YftI[l]*sin(Va[b]-Va[busIdx[lines[l].to]] ) + YftR[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  )) for l in FromLines[b] if b == j ) +
	sum( Vm[b] * Vm[busIdx[lines[l].to]]* ( -YftI[l]*sin(Va[b]-Va[busIdx[lines[l].to]] ) -YftR[l]*cos(Va[b]-Va[busIdx[lines[l].to]]  )) for l in FromLines[b] if busIdx[lines[l].to] == j ) +
	sum( Vm[b]*Vm[busIdx[lines[l].from]] * ( YtfI[l]*sin(Va[b]-Va[busIdx[lines[l].from]] ) + YtfR[l]*cos(Va[b]-Va[busIdx[lines[l].from]]  )) for l in ToLines[b] if b == j) +
	sum( Vm[b] * Vm[busIdx[lines[l].from]] * ( -YtfI[l]*sin(Va[b]-Va[busIdx[lines[l].from]] ) -YtfR[l]*cos(Va[b]-Va[busIdx[lines[l].from]]  )) for l in ToLines[b] if busIdx[lines[l].from] == j))

	# Assume model is correct until this point

	# PF: ... - (sum(Qg) - Qd)
	# Equality constraints on sensitivities

	o2nb 	= ones(2*nbus,1);
	I2nb 	= diagm(0 => o2nb[:]);

	#rhs1_ 	= I2nb[nrefidx,ldidx];
	rhs1_ 	= I2nb[1:nbus,ldidx];
	#rhs2_ 	= I2nb[ngen.+nrefidx,ngen.+ldidx];
	rhs2_ 	= I2nb[(nbus+1):(2*nbus),nbus.+ldidx];

	#zl 		= zeros(length(nrefidx),nload);
	zl 		= zeros(nbus,nload);

	rhs1 	= cat(rhs1_,zl,dims=2);
	rhs2 	= cat(zl,rhs2_,dims=2);

	rhs 	= [rhs1;rhs2];

	# Debugging
	#@info "rhs1" rhs1
	#@info "rhs2" rhs2

	dFdQ 	= I2nb[(nbus+1):2*nbus,nbus.+nldidx];


	# Dictionaries to map Jacobian columns to Gamma rows
	QidxGam 	= Dict{Int}{Int}();
	VmidxGam 	= Dict{Int}{Int}();
	VaidxGam 	= Dict{Int}{Int}();

	# Modify indexing of sensitivities based on the permutation
	#
	# P = 	| 0		0	Ig  |
	#		| Il 	0	0	|
	#		| 0 	Igl 0	|
	#
	#
	for i in 1:nload
		VmidxGam[ldidx[i]] 		= i;
	end
	for i in 1:length(nrefidx)
		VaidxGam[nrefidx[i]] 	= nload + i;
	end
	for i in 1:length(gidx)
		QidxGam[gidx[i]] 		= length(nrefidx) + nload + i;
	end

	# Real sensitivity constraints
	# for i in 1:nbus
	# 	if i != refidx
	# 		for j in 1:jaccols
	#
	# 			@NLconstraint(opfmodel,
	# 			sum(jacVmP[i,l] * GM[VmidxGam[l],j] for l in ldidx ) +
	# 			sum(jacVaP[i,l] * GM[VaidxGam[l],j] for l in nrefidx ) + rhs1[i,j] == 0)
	#
	# 		end
	# 	end
	# end
	#
	# # Imaginary sensitivity constraints
	# for i in 1:nbus
	# 	if i != refidx
	# 		for j in 1:jaccols
	#
	# 			@NLconstraint(opfmodel,
	# 			#sum(-dFdQ[i,QidxGam[l]] * GM[QidxGam[l],j] for l in gidx ) +
	# 			# 05/14/19, J.B., explicitly assumes one generator at bus
	# 			sum(-dFdQ[i,BusGeners[l][1]] * GM[QidxGam[l],j] for l in gidx ) +
	# 			sum(jacVmQ[i,l] * GM[VmidxGam[l],j] for l in ldidx ) +
	# 			sum(jacVaQ[i,l] * GM[VaidxGam[l],j] for l in nrefidx ) + rhs2[i,j] == 0)
	#
	# 		end
	# 	end
	# end

	# LU factorization of permuted Jacobian. The Jacobian is in 6 blocks
	# No. Cols	  ngen   nload 	ngen+nload
	#	 J =	| 0 	VmP 	VaP |
	# 			| dQ 	VmQ 	VaQ |
	#
	#
	# The permuted Jacobian is factored:
	#	 J P =	| VmP 	VaP 	0  | = L U
	# 			| VmQ 	VaQ 	dQ |
	#
	# where P is a permutation matrix.

	nj 		= 2*(nbus-1);

	LU 		= Array{Union{Float64,JuMP.NonlinearExpression},2}(undef, nj, nj);
	#JP 		= Array{Union{Float64,JuMP.NonlinearExpression},2}(undef, nj, nj);

	JuMP.registercon(opfmodel,:LU,LU);

	# Form permuted Jacobian J P.
	# Select all other generators among generator indices except reference bus
	gensel 		= Vector(1:ngen);
	gensel_r 	= gensel[BusGeners[refidx][1].!=gensel[:]];

	JP 			= [jacVmP[nrefidx,ldidx] jacVaP[nrefidx,nrefidx] zl[nrefidx,(1:length(gidx))];
					jacVmQ[nrefidx,ldidx] jacVaQ[nrefidx,nrefidx] -dFdQ[nrefidx,gensel_r]];

	@info "JP" JP;



	# Factorization loops. The factorization is column-wise for the L part and row-wise
	# for U part.
	# Outer loop
	for j in 1:nj

		# Inner loop
		for i in j:nj

			# Upper triangular
			LU[j,i] = @NLexpression(opfmodel, JP[j,i] - sum(LU[j,k]*LU[k,i] for k in 1:(j-1)) )

			# Lower triangular
			if i > j

				LU[i,j] = @NLexpression(opfmodel, (JP[i,j] - sum(LU[k,j]*LU[i,k] for k in 1:(j-1)))/LU[j,j] )

			end
		end
	end

	# Solving triangular systems
	YJ  = Array{Union{Float64,JuMP.NonlinearExpression},2}(undef, nj, jaccols);
	GM  = Array{Union{Float64,JuMP.NonlinearExpression},2}(undef, nj, jaccols);

	JuMP.registercon(opfmodel,:YJ,YJ);
	JuMP.registercon(opfmodel,:GM,GM);

	# Step 1: Compute L Y = B
	# Outer loop, columns
	for j in 1:jaccols

	    # Inner loop, rows
	    for i in 1:nj

	        YJ[i,j] = @NLexpression(opfmodel, (-rhs[i,j] - sum(LU[i,k]*YJ[k,j] for k in 1:(i-1))) )

	    end
	end

	# Step 2: Compute U X = Y
	# Outer loop, columns
	for j in 1:jaccols

	    # Inner loop, rows, moving in reverse
	    for i in 1:nj

	        ir       = nj+1-i;

	        GM[ir,j] = @NLexpression(opfmodel, (YJ[ir,j] - sum(LU[ir,(nj+1-k)]*GM[(nj+1-k),j] for k in 1:(i-1)))/LU[ir,ir] )

	    end
	end


	# Debugging

	# @info "gensel_r" gensel_r
	# @info "JP" JP
	# @info "-dFqQ" -dFdQ[nrefidx,gensel_r]
	# @info "LU" LU
	# @info "rhs" rhs

	# # Factorization loops. The factorization is column-wise for L and row-wise
	# # for U.
	# # Outer loop
	# for j in 1:nj
	#
	#       	# Inner loop
	#       	for i in j:(2*nbus)
	#
	#
	#
	#
	#
	#             # Upper triangular
	#             LUJ[j,i] = @NLexpression(mldl, AJ[j,i] - sum(LUJ[j,k]*LUJ[k,i] for k in 1:(j-1)) )
	#
	#             # Lower triangular
	#             if i > j
	#
	#                 LUJ[i,j] = @NLexpression(mldl, (AJ[i,j] - sum(LUJ[k,j]*LUJ[i,k] for k in 1:(j-1)))/LUJ[j,j] )
	#
	#             end
	#         end
	# end



	sigidx 						= Array{Int}(undef,2*nload);
	sigidx[1:nload] 			= ldidx[1:nload];
	sigidx[(nload+1):2*nload] 	= nbus .+ ldidx[1:nload];

	Sig_r 						= diag(Sig_d[sigidx,sigidx], 0);

	# Chance constraints
	# Modification
	# Va max (cf. cc_acopf_model.jl)
	@constraintref cc_Va_max[1:length(nrefidx)]
	#gamma = 1.0;

	#vareps = 0.9;
	q 		= quantile(Z,vareps_Va);
	imax 	= 1;
	for l in nrefidx

		Val 	= Va[l];
		#vareps 	= 1.0 - (gamma * epsilon_Va) / (nbus-1);


		#@info "q Va max" q
		#@info "vareps" vareps
		#@NLconstraint(opfmodel, sum( GM[VaidxGam[l],k]*GM[VaidxGam[l],k]*Sig_r[k] for k in 1:(2*nload) ) <= (Va_max[l] - Val) / q )
		#@NLconstraint(opfmodel, (Val - Va_max[l]) / sum( GM[VaidxGam[l],k]*GM[VaidxGam[l],k]*Sig_r[k] for k in 1:(2*nload) ) <= -q )
		#@NLconstraint(opfmodel, (Val - Va_max[l]) <= ccpar_Va * -q * sqrt(sum( GM[VaidxGam[l],k]*GM[VaidxGam[l],k]*Sig_r[k] for k in 1:jaccols)))
		cc_Va_max[imax] = @NLconstraint(opfmodel, (Val - Va_max[l]) <= ccpar_Va * -q * sqrt(sum( GM[VaidxGam[l],k]*GM[VaidxGam[l],k]*Sig_r[k] for k in 1:jaccols)))

		imax = imax + 1;

	end
	JuMP.registercon(opfmodel, :cc_Va_max, cc_Va_max)
	#
	# Va min (cf. cc_acopf_model.jl)

	@constraintref cc_Va_min[1:length(nrefidx)]

	q1 		= quantile(Z,(1-vareps_Va));
	imin 	= 1;
	for l in nrefidx

		Val 	= Va[l];

		#@NLconstraint(opfmodel, sum( GM[VaidxGam[l],k]*GM[VaidxGam[l],k]*Sig_r[k] for k in 1:(2*nload) ) <= (Va_min[l] - Val) / q )
		#@NLconstraint(opfmodel, (Va_min[l] - Val) <= ccpar_Va * q1 * sqrt(sum( GM[VaidxGam[l],k]*GM[VaidxGam[l],k]*Sig_r[k] for k in 1:jaccols)))

		cc_Va_min[imin] = @NLconstraint(opfmodel, (Va_min[l] - Val) <= ccpar_Va * q1 * sqrt(sum( GM[VaidxGam[l],k]*GM[VaidxGam[l],k]*Sig_r[k] for k in 1:jaccols)))

		imin = imin + 1;

	end
	JuMP.registercon(opfmodel, :cc_Va_min, cc_Va_min)

	#Vm chance constraints, Vm at load buses
	#Upper bounds using Vmax
@constraintref cc_Vm_max[1:length(ldidx)]

	q 		= quantile(Z,vareps_Vm);
	imax 	= 1;
	for l in ldidx

		Vml 	= Vm[l];
		Vm_max 	= buses[l].Vmax;

		cc_Vm_max[imax] = @NLconstraint(opfmodel, (Vml - Vm_max) <= -q * sqrt(sum( GM[VmidxGam[l],k]*GM[VmidxGam[l],k]*Sig_r[k] for k in 1:jaccols)))

		imax = imax + 1;

	end

	JuMP.registercon(opfmodel, :cc_Vm_max, cc_Vm_max)

	# Vm chance constraints, Vm at load buses
	# Lower bounds using Vmin
@constraintref cc_Vm_min[1:length(ldidx)]

	q1 		= quantile(Z,1-vareps_Vm);
	imin 	= 1;
	for l in ldidx

		Vml 	= Vm[l];
		Vm_min 	= buses[l].Vmin;

		cc_Vm_min[imin] = @NLconstraint(opfmodel, (Vm_min - Vml) <= q1 * sqrt(sum( GM[VmidxGam[l],k]*GM[VmidxGam[l],k]*Sig_r[k] for k in 1:jaccols)))

		imin = imin + 1;

	end

	JuMP.registercon(opfmodel, :cc_Vm_min, cc_Vm_min)

	# Qg chance constraints, Qg at generator buses
	# Upper bounds using Qmax.

	@constraintref cc_Qg_max[1:ngen]

	#genidx 	= opf_data.bus_ref + 1;
	q 		= quantile(Z,vareps_Qg);

	imax = 1;
	for l in gidx

		#genidxCol = BusGeners[l];

		genidx = BusGeners[l][1];

		#for genidx in genidxCol

			Qgg 	= Qg[genidx];
			Qg_max 	= generators[genidx].Qmax;

			cc_Qg_max[imax] = @NLconstraint(opfmodel, (Qgg - Qg_max) <= -q * sqrt(sum( GM[QidxGam[l],k]*GM[QidxGam[l],k]*Sig_r[k] for k in 1:jaccols)))
			#setupperbound(Qg[genidx],(Qg_max-q * sqrt(sum( GM[QidxGam[l],k]*GM[QidxGam[l],k]*Sig_r[k] for k in 1:(2*nload)))))

			#genidx = genidx + 1;
			imax = imax + 1;

		#end

	end

	JuMP.registercon(opfmodel, :cc_Qg_max, cc_Qg_max)

	# Lower bounds using Qmin. Assumes at most
	# one generator per bus

	@constraintref cc_Qg_min[1:ngen]

	#genidx 	= opf_data.bus_ref + 1;
	q1 		= quantile(Z,1-vareps_Qg);

	imin 	= 1;
	for l in gidx

		#genidxCol 	= BusGeners[l];

		genidx 		= BusGeners[l][1];

		#for genidx in genidxCol

			Qgg 	= Qg[genidx];
			Qg_min 	= generators[genidx].Qmin;

			cc_Qg_min[imin] = @NLconstraint(opfmodel, (Qg_min - Qgg) <= -q1 * sqrt(sum( GM[QidxGam[l],k]*GM[QidxGam[l],k]*Sig_r[k] for k in 1:jaccols)))

			#genidx = genidx + 1;
			imin = imin + 1;

		#end

	end

	JuMP.registercon(opfmodel, :cc_Qg_min, cc_Qg_min)



	@printf("Buses: %d  Lines: %d  Generators: %d\n", nbus, nline, ngen)
	println("Lines with limits  ", nlinelim)

	return OPFModel(opfmodel, :InitData, :S)
end
