############## CC-AC-OPF (Chance-Constrained AC-Optimal Power Flow) ############

# cc_acopf_modelC_v1.jl is an initial implementation to compute a cc model that
# uses an iterative approach by first solving a conventional ACOPF model and then
# calculating chance constraints iteratively

# the model will calculate gamma and use that to get new bounds which will then
# be put back into the ACOPF problem until all bounds converge within certain
# tolerances

################################################################################
# 06/11/19, E.G.

function cc_acopf_modelC_v1(opfdata, options::Dict=Dict(), data::Dict=Dict())

	# initialize data
    epsilon_Va  = haskey(options, :epsilon_Va)   ? options[:epsilon_Va]   : 0.05
	gamma       = haskey(options, :gamma)        ? options[:gamma]        : 1.0
	print_level = haskey(options, :print_level)  ? options[:print_level]  : 0

	vareps_Va 	= haskey(options, :vareps_Va)    ? options[:vareps_Va]    : 0.9
	vareps_Va   = 0.9
	vareps_Vm 	= haskey(options, :vareps_Vm)    ? options[:vareps_Vm]    : 0.9
	vareps_Vm   = 0.9
	vareps_Qg 	= haskey(options, :vareps_Qg)    ? options[:vareps_Qg]    : 0.9
	vareps_Qg   = 0.9

	ccpar_Va 	= haskey(options, :ccpar_Va)     ? options[:ccpar_Va]     : 1.0

	jaccols 	= haskey(options, :jaccols)      ? options[:jaccols]      : 2*nload

	Sigma_d    	= haskey(data, :Sigma_d) 		 ? data[:Sigma_d] 		  : Matrix(Diagonal(ones(2nbus)))
	Va_min 		= haskey(data, :Va_min)  		 ? data[:Va_min]  		  : -pi * ones(nbus)
	Va_max 		= haskey(data, :Va_max)  		 ? data[:Va_max]  		  :  pi * ones(nbus)

	Z 			= Normal(0,1)

	nbus        = length(opfdata.buses);
	nload       = sum(opfdata.buses.bustype.==1);
	BusGeners 	= opfdata.BusGenerators;
	generators 	= opfdata.generators;
	buses 		= opfdata.buses;

	# Define a 'classical' OPF problem, and solve it
	sm = OPF.acopf_model(opfdata)
	sm = OPF.acopf_solve(sm, opfdata)
	println("\nInitial output: ")
	OPF.acopf_outputAll(sm.m,:D,opfdata);

	## set OPF variables
	#println("Setting initial point for CC-ACOPF.")
	Pg_bar = getvalue(getindex(sm.m, :Pg))
	Qg_bar = getvalue(getindex(sm.m, :Qg))
	Vm_bar = getvalue(getindex(sm.m, :Vm))
	Va_bar = getvalue(getindex(sm.m, :Va))
	#Pd_bar = getvalue(getindex(sm.m, :Pd))
	#Qd_bar = getvalue(getindex(sm.m, :Qd))

	# compute initial jacobian and gamma matrices
	jacmb 							= OPF.jacobian_modelB_v1(opfdata,Vm_bar,Va_bar);

	refIdx, genIdx, ldIdx, nrefIdx 	= OPF.RGL_idx_mb(opfdata);

	#nrefgenIdx 						= genIdx[genIdx .!= refIdx];

	nbus 							= length(opfdata.buses);
	ngen 							= length(opfdata.generators);

	genidxs                         = 1:ngen;
	genref                          = opfdata.BusGenerators[refIdx];
	genidxsNref                     = genidxs[genidxs.!=genref];

	rows 							= vcat(nrefIdx,nbus .+ nrefIdx);
	#cols 							= vcat(ngen .+ genIdx, (2*ngen .+ ldIdx), (2*ngen + nbus .+ nrefIdx)); # Assume reference at index 1
	cols 							= vcat((ngen .+ genidxsNref), (2*ngen .+ ldIdx), (2*ngen + nbus .+ nrefIdx));
	colsp 							= vcat(2*(ngen+nbus) .+ ldIdx, 2*(ngen+nbus) + nbus .+ ldIdx );

	# solve for gamma matrix, J(x)Γ = -E
	Jx 							    = jacmb[rows,cols];
	E 							    = jacmb[rows,colsp];
	Γ 						        = Jx \ -E;

	#### Computation of lambdas ####

	# dictionaries to keep track of indices
	QidxGam 	= Dict{Int}{Int}();
	VmidxGam 	= Dict{Int}{Int}();
	VaidxGam 	= Dict{Int}{Int}();

	for i in 1:length(genIdx)
		QidxGam[genIdx[i]] = i;
	end

	for i in 1:nload
		VmidxGam[ldIdx[i]] 	= length(genIdx) + i;
	end

	for i in 1:length(nrefIdx)
		VaidxGam[nrefIdx[i]] = nload + length(genIdx) + i;
	end

	qQg 		= quantile(Z,vareps_Qg);
	qVm 		= quantile(Z,vareps_Vm);
	qVa 		= quantile(Z,vareps_Va);

	mIdxQg 		= getindex(sm.m, :Qg);
	mIdxVm 		= getindex(sm.m, :Vm);
	mIdxVa 		= getindex(sm.m, :Va);

	# Arrays to keep track of the previous lambdas
	lambdaQgP 	= zeros(length(genIdx),1)
	lambdaVmP 	= zeros(length(ldIdx),1)
	lambdaVaP 	= zeros(length(nrefIdx),1)

	# while loop
	counter = 1;
	converged = false
	while converged != true

		# LambdaQg
		lambdaQg 	= zeros(length(genIdx),1);
		for i in genIdx

			nsqGQg = 0;
			for k in 1:jaccols

				nsqGQg = nsqGQg + Γ[QidxGam[i],k]*Sigma_d[k,k]*Γ[QidxGam[i],k];
			end

			nsqGQg 			= sqrt(nsqGQg);
			lambdaQg[QidxGam[i]] = qQg*nsqGQg;
		end

		# LambdaVm
		lambdaVm 	= zeros(nload,1);

		for i in 1:length(ldIdx)

			nsqVml = 0;
			for k in 1:jaccols

				nsqVml = nsqVml + Γ[VmidxGam[ldIdx[i]],k]*Sigma_d[k,k]*Γ[VmidxGam[ldIdx[i]],k];
			end

			nsqVml 			= sqrt(nsqVml);
			lambdaVm[i] 	= qVm*nsqVml;
		end

		# LambdaVa
		lambdaVa 	= zeros(length(nrefIdx),1);

		for i in 1:length(nrefIdx)

			nsqVa = 0;
			for k in 1:jaccols

				nsqVa = nsqVa + Γ[VaidxGam[nrefIdx[i]],k]*Sigma_d[k,k]*Γ[VaidxGam[nrefIdx[i]],k];
			end

			nsqVa 			= sqrt(nsqVa);
			lambdaVa[i] 	= qVa*nsqVa;
		end

		for i in 1:length(genIdx)

			boundLow 	= generators[BusGeners[genIdx[i]][1]].Qmin + lambdaQg[i];
			boundUp 	= generators[BusGeners[genIdx[i]][1]].Qmax - lambdaQg[i];

			setlowerbound(mIdxQg[BusGeners[genIdx[i]][1]],boundLow);
			setupperbound(mIdxQg[BusGeners[genIdx[i]][1]],boundUp);

		end

		for i in 1:length(ldIdx)

			boundLow 	= buses[ldIdx[i]].Vmin + lambdaVm[i];
			boundUp 	= buses[ldIdx[i]].Vmax - lambdaVm[i];

			setlowerbound(mIdxVm[ldIdx[i]],boundLow);
			setupperbound(mIdxVm[ldIdx[i]],boundUp);

		end

		for i in 1:length(nrefIdx)

			boundLow 	= Va_min[nrefIdx[i]] + lambdaVa[i];
			boundUp 	= Va_max[nrefIdx[i]] - lambdaVa[i];

			setlowerbound(mIdxVa[nrefIdx[i]],boundLow);
			setupperbound(mIdxVa[nrefIdx[i]],boundUp);

		end

		##### Re-solve modified problem #####

		println("Resolving . . . ")
		OPF.acopf_solve(sm, opfdata)


		# check if model is infeasible
		# if(status != :Optimal)
		# 	println("Ending loop . . . ")
		# 	converged = true
		# end

		println("Objective value: ", getobjectivevalue(sm.m), "USD/hr")

		Vm_bar = getvalue(getindex(sm.m, :Vm))
		Va_bar = getvalue(getindex(sm.m, :Va))

		# compute jacobian
		jacmb  = OPF.jacobian_modelB_v1(opfdata,Vm_bar,Va_bar)
		Jx 	   = jacmb[rows,cols]

		# solve for gamma matrix, J(x)Γ = -E
		Γ      = - Jx \ E

		##### test for convergence ####
		# subtract previous lambdas from current lambdas

		# add absolute values in find max

		if counter > 1
			# lambdaQgs
			diffQg = abs.(lambdaQg - lambdaQgP)
			#println(diffQg)
			# lambdaVms
			diffVm = abs.(lambdaVm - lambdaVmP)
			#println(diffVm)
			# lambdaVas
			diffVa = abs.(lambdaVa - lambdaVaP)
			#println(diffVa)

			# find max differences
			maxdiffQg = findmax(diffQg)
			maxdiffVm = findmax(diffVm)
			maxdiffVa = findmax(diffVa)

			# print final max lambdas
			println("maxdiffQg :", maxdiffQg[1])
			println("maxdiffVm :", maxdiffVm[1])
			println("maxdiffVa :", maxdiffVa[1])

			# see if they converge to certain tolerances
			# all of them (Vm,Va,Qg) must be within tolerance to change converged
			# to true and end the loop
			if maxdiffQg[1] < 0.001 && maxdiffVm[1] < 0.00001 && maxdiffVa[1] < 0.00001 # these are the tolerances
				converged = true
				# # print final max lambdas
				# println("maxdiffQg :", maxdiffQg[1])
				# println("maxdiffVm :", maxdiffVm[1])
				# println("maxdiffVa :", maxdiffVa[1])
			end
		end

		# # testing
		# println("\nQgP :",lambdaQgP)
		# println("VmP :",lambdaVmP)
		# println("VaP :\n",lambdaVaP)
		#
		# println("\nQg :",lambdaQg)
		# println("Vm :",lambdaVm)
		# println("Va :\n",lambdaVa)

		# set new previous lambdas
		lambdaQgP = lambdaQg
		lambdaVmP = lambdaVm
		lambdaVaP = lambdaVa

		counter = counter + 1

		# terminate loop
		println(counter)
		if(counter > 10)
			converged = true
		end
	end # end while loop

	# final print out
	println("\nFinal output: ")
	# OPF.acopf_outputAll(sm.m,:D,opfdata)
end # end funciton
