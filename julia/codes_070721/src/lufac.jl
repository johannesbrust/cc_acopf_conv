########################## lufac ###############################################
#
# LU factorization of a 'invertible' matrix A:
#
# A = LU,
#
# where L is lower-triangular and U is upper-triangular.
#
# This function forms the factorization row-wise for U and column-wise for L.
#
# INPUT:
# A := Array (n x n), full matrix,
#
# OUTPUTS:
# L := Array (n x n), Unit diagonal lower triangular matrix.
# U := Array (n x n), Upper triangular matrix.
#
################################################################################
# 06/12/19, J.B.

function lufac(A)

    n = size(A,1);

    U = zeros(n,n);
    L = zeros(n,n);

    # Outer loop
    for j in 1:n

        # D[j,1] = A[j,j];

        # Inner loop
        for i in j:n


            U[j,i] = A[j,i];
            L[i,j] = A[i,j];


            # Element computations
            for k in 1:(j-1)

                U[j,i] = U[j,i] - (L[j,k]*U[k,i]);

                L[i,j] = L[i,j] - (U[k,j]*L[i,k]);



                # if i == j
                #
                #     D[j,1] = D[j,1] - D[k,1]*(L[j,k]*L[i,k])
                #
                # end

            end

            # if i == j
            #
            #     D[j,1] = L[j,j];
            #
            # end

            L[i,j] = L[i,j]/U[j,j];

        end

    end

    return L, U;

end
