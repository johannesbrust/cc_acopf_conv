module OPF
using JuMP, JuMPUtil, Ipopt, MathProgBase
using SparseArrays, LinearAlgebra, Distributions, NLsolve
using MPCCases
using Printf
using Pkg
## based on: https://github.com/StructJuMP/StructJuMP.jl/tree/master/examples/PowerGrid

mutable struct OPFModel
    m::JuMP.Model
    status::Symbol
    kind::Symbol
end
export OPFModel

include("acopf_model.jl")
export acopf_model

include("s_acopf_model.jl")
export s_acopf_model

include("cc_acopf_model.jl")
export cc_acopf_model

# LD model
include("ld_acopf_model.jl")
export ld_acopf_model

# CC Model B
include("cc_acopf_modelB_v1.jl")
export cc_acopf_modelB_v1

include("cc_acopf_modelB_v2.jl")
export cc_acopf_modelB_v2

include("cc_acopf_modelB_v3.jl")
export cc_acopf_modelB_v3

include("jacobian_modelB_v1.jl")
export jacobian_modelB_v1

## CC Model C
include("cc_acopf_modelC_v1.jl")
export cc_acopf_modelC_v1

include("cc_acopf_modelC_EXIII.jl")
export cc_acopf_modelC_EXIII

include("util.jl")
export acopf_solve, acopf_initialPt_IPOPT
export cc_acopf_solve
export acopf_outputAll, get_values
export RGL_id, RGL_idx, model_idx
export PQnet
export ld_acopf_parsensitivity

include("pfe.jl")
export P_i, Q_i
export PF, PFE, PFE!, PFE_J!
export PF_vec, PFE_vec, PFE_vec!

include("jacobian.jl")
export jac_z_num
export jac_z_alg_ew
export dStilde_dVtilde, jac_z_alg_vec
export jac_z

include("sensitivities.jl")
export get_Gamma, get_Gamma_fd, get_Gamma_ew

end # module
