# README #

repo contains code to build chance-constrained ACOPF `JuMP` models in `Julia`
* `src/`: source code
* `docs/`: LaTex source files
* `test/`: tests and examples for running source
