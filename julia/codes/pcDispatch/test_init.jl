################## PC-DISPATCH (probability constrained dispatch) #############
#
# Probability constrained dispatch is intended as an extension to optimal
# power flow problems (OPF).
#
# Script to prepare computation of Jacobian of power flow equations.
#
###############################################################################
# Initial version: 03/29/19, J.B.

include("acopf.jl")

opfdata     = opf_loaddata("data/case9")
opfmodel    = acopf_model(opfdata)

# Breakpoint for debugging
busIdx      = opfdata.BusIdx
