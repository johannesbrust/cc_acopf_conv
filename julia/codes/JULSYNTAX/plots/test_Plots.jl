############################# test_Plots #######################################
#
# Script to test Plots.jl Julia package.
# Based on: http://docs.juliaplots.org/latest/tutorial/#tutorial-1
#
################################################################################
# 05/16/19, J.B.

import Pkg

Pkg.add("Plots")

using Plots

x = 1:0.5:10;
#x = range(1.0,step=1.0,stop=10.0);
#y = sin.(Vector(x));
y = sin.(x);
tp = plot(x,y,label="sin(x)",lw=3);

# Add series
plot!(tp,x,cos.(x),label="cos(x)",lw=3);

title!(tp,"Test Trigonometic");
xlabel!(tp,"x");

# Save
savefig("plot_test.pdf");

display(tp);



# Add label
#label!(["sin(x)", "cos(x)"]);
