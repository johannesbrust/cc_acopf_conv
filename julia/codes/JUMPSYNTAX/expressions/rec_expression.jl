############################### rec_expression #################################
#
# Script to test accessing elements of a JuMP expression within the definition
# of the expression.
#
################################################################################
# 05/22/19, J.B.

#using Pkg
# This makes JuMP v0.19 available
using JuMP
using Ipopt

# Model
m = Model(with_optimizer(Ipopt.Optimizer));

# Test NLexpression.
#@NLexpression(m,rec[i=1:10,j=1:10], sum( 1 for k=1 if j==1 ) + sum( rec[i,k-1] for k=1:(j-1)) )
