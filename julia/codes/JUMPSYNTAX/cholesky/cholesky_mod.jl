########################## cholesky_mod ########################################
#
# Cholesky factorization of a symmetric positive definite (spd) matrix A:
#
# A = R'R,
#
# where R is upper-triangular.
#
# This function forms the factorization column wise, and does not modify the input
# matrix.
#
# INPUT:
# A := Array (n x n), Lower triangular part of a spd matrix,
#
# OUTPUT:
# RT := Array (n x n), Lower triangular matrix, s.t. A on output corresponds to R'
#
################################################################################
# 05/30/19, J.B.

function cholesky_mod(A)

    n   = size(A,1);
    RT  = zeros(n,n);

    # Column loop
    for j in 1:n

        rjj = A[j,j];

        # Row loop
        for i in j:n

            RT[i,j] = A[i,j];

            # Inner column loop
            for k in 1:(j-1)

                RT[i,j] = RT[i,j] - (RT[j,k]*RT[i,k]);

                rjj     = rjj - RT[j,k]*RT[j,k];

            end

            rjj     = sqrt(rjj);

            RT[i,j] = RT[i,j]/rjj;

        end

    end

    return RT;

end
