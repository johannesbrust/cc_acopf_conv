######################### test_cholesky ########################################
#
# Script to test the cholesky.jl function and various modifications.
#
################################################################################
# 05/30/19, J.B.

using LinearAlgebra;

include("cholesky.jl");
include("cholesky_mod.jl");
include("ldlt.jl");

n   = 10;

A   = randn((n,n));
A   = transpose(A)*A+I;

AL  = zeros(n,n);

# Lower triangular part of A
# for i in 1:n
#
#     AL[i:n,i] = A[i:n,i];
#
# end

AL      = tril(A,0);
AL_m    = tril(A,0);


#RT      = cholesky(AL);

#RTm = cholesky_mod(AL_m);

D,L     = ldlt(AL_m);

# Errors/ Not small

#err     = norm(RT*transpose(RT)-A,2);
#errm    = norm(RTm*transpose(RTm)-A,2);
errl    = norm(L*diagm(0 => D[:,1])*transpose(L)-A,2);
