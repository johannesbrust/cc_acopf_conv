######################### test_lufac ###########################################
#
# Script to test the lufac.jl function, and solving with it.
#
################################################################################
# 06/11/19, J.B.

using LinearAlgebra;

#include("cholesky.jl");
#include("cholesky_mod.jl");
#include("ldlt.jl");

include("lufac.jl");
include("solve_lufac.jl");

n       = 10;
m       = 3;

A       = randn((n,n));
B       = randn((n,m));

L,U     = lufac(A);
X       = solve_lufac(L,U,B);

# Errors

errl    = norm(L*U-A,2);
errs    = norm(X-(A\B),2);
