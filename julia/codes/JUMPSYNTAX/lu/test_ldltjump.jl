################################# test_ldltjump ################################
#
# Test script for the ldlt factorization within a JuMP model
#
################################################################################
# 04/06/19, J.B.
# 04/06/19, J.B., initial version is experimental and includes commented parts.

using JuMP;
using Ipopt;
using Pkg;
using LinearAlgebra;
import LinearAlgebra.ldlt;

include("ldlt.jl");

#Pkg.rm("JuMP");
#Pkg.add("JuMP@0.18.0");

# Initializations
n   = 10;

# Input data
Ad = randn(n,n);
AD = transpose(Ad)*Ad;

# Arrays of non-linear expressions, for a decomposition of the form
# AJ = LJ DJ LJ'
LJ  = Array{Union{Float64,JuMP.NonlinearExpression},2}(undef, n, n);
DJ  = Array{Union{Float64,JuMP.NonlinearExpression},2}(undef, n, 1);


# JuMP model, v.18
mldl = Model(solver=IpoptSolver(print_level=0));

# JuMP model, v.19
# mldl = Model(with_optimizer(Ipopt.Optimizer));

# Define array variable/or array non-linear expression
@variable(mldl,AJ[i=1:n,j=1:n]);
#AJ  = Array{Union{Float64,JuMP.NonlinearExpression},2}(undef, n, n);

#JuMP.registercon(mldl,:AJ,AJ);
JuMP.registercon(mldl,:DJ,DJ);
JuMP.registercon(mldl,:LJ,LJ);

# Factorization loops. The factorization is column-wise. Modify the non-linear expressions.
for j in 1:n
    # Row loop
    for i in j:n

        if i == j

            DJ[j] = @NLexpression(mldl, AJ[i,j] - sum(DJ[k]*(LJ[j,k]*LJ[i,k]) for k in 1:(j-1)) );

        end

        LJ[i,j] = @NLexpression(mldl, (AJ[i,j] - sum(DJ[k]*(LJ[j,k]*LJ[i,k]) for k in 1:(j-1)))/DJ[j] );

    end
end

# Set array variable to data, then retrieve DJ and LJ
setvalue(getindex(mldl, :AJ), AD);

DJv = getvalue(getindex(mldl, :DJ));
#LJv = getvalue(getindex(mldl, :LJ));
