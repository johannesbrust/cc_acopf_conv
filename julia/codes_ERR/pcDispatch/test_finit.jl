################## PC-DISPATCH (probability constrained dispatch) #############
#
# Probability constrained dispatch is intended as an extension to optimal
# power flow problems (OPF).
#
# Function to prepare computation of Jacobian of power flow equations.
# This is intended for testing of debugging methods.
#
###############################################################################
# Initial version: 03/29/19, J.B.

include("acopf.jl")

function test_finit(case)

    opfdata     = opf_loaddata(case)
    opfmodel    = acopf_model(opfdata)

    busIdx      = opfdata.BusIdx

end
