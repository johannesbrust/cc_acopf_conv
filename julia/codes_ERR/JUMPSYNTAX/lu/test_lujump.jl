################################# test_lujump ##################################
#
# Test script for the lu factorization within a JuMP model
#
################################################################################
# 18/06/19, J.B.

using JuMP;
using Ipopt;
using Pkg;
using LinearAlgebra;

# Include functions w/o JuMP for comparisons
include("lufac.jl");
include("solve_lufac.jl");

#Pkg.rm("JuMP");
#Pkg.add("JuMP@0.18.0");

# Initializations
n   = 10;
m   = 3;

# Input data (A Data). The input matrix need to be positive
# definite, square.
AD  = randn(n,n);

# Right hand side for system: AD X = B.
B   = randn(n,m);
#AD = transpose(Ad)*Ad;

# Arrays of nonlinear expressions, for a decomposition of the form
# AJ = LJ UJ, where LJ is unit lower triangular and UJ is upper triangular.
# The decomposition is stored in one square matrix LUJ = LJ + UJ,
# where the diagonal entries are those from UJ.
LUJ     = Array{Union{Float64,JuMP.NonlinearExpression},2}(undef, n, n);
#BJ      = Array{Union{Float64,JuMP.NonlinearExpression},2}(undef, n, m);

# Arrays for the solution of triangular systems.
# YJ stores the first triangular solve:     LJ YJ = B,
# XJ stores the second triangular solve:    UJ XJ = YJ (This is the solution)
YJ  = Array{Union{Float64,JuMP.NonlinearExpression},2}(undef, n, m);
XJ  = Array{Union{Float64,JuMP.NonlinearExpression},2}(undef, n, m);

# JuMP model, v.18
mldl = Model(solver=IpoptSolver(print_level=0));

# JuMP model, v.19
# mldl = Model(with_optimizer(Ipopt.Optimizer));

# Define array variable/or array non-linear expression
@variable(mldl,AJ[i=1:n,j=1:n]);
#AJ  = Array{Union{Float64,JuMP.NonlinearExpression},2}(undef, n, n);

#JuMP.registercon(mldl,:AJ,AJ);
JuMP.registercon(mldl,:LUJ,LUJ);
JuMP.registercon(mldl,:YJ,YJ);
JuMP.registercon(mldl,:XJ,XJ);
#JuMP.registercon(mldl,:BJ,BJ);

# Factorization loops. The factorization is column-wise for LJ and row-wise
# for UJ.
# Outer loop
for j in 1:n

        # Inner loop
        for i in j:n

            # Upper triangular
            LUJ[j,i] = @NLexpression(mldl, AJ[j,i] - sum(LUJ[j,k]*LUJ[k,i] for k in 1:(j-1)) )

            # Lower triangular
            if i > j

                LUJ[i,j] = @NLexpression(mldl, (AJ[i,j] - sum(LUJ[k,j]*LUJ[i,k] for k in 1:(j-1)))/LUJ[j,j] )

            end
        end
end

# Set array variable to data, then retrieve LUJ
setvalue(getindex(mldl, :AJ), AD);
#setvalue(getindex(mldl, :AJ), AD);

LUv = getvalue(getindex(mldl, :LUJ));
#LJv = getvalue(getindex(mldl, :LJ));

# Compute LU factorization.
Lc,Uc = lufac(AD);

# Retrieve Lm, Um as model matrices for error
# calculations.
Lm = diagm(0=>ones(n));
Um = zeros(n,n);

for i in 1:n
    for j in i:n

        Um[i,j] = LUv[i,j];

        if j != i

            Lm[j,i] = LUv[j,i];

        end

    end
end

# Errors triangular matrices
errl = norm(Lm-Lc,2);
erru = norm(Um-Uc,2);

# Triangular solves

# Step 1: Compute L Y = B
# Outer loop, columns
for j in 1:m

    # Inner loop, rows
    for i in 1:n

        YJ[i,j] = @NLexpression(mldl, (B[i,j] - sum(LUJ[i,k]*YJ[k,j] for k in 1:(i-1))) )

    end
end

# Step 2: Compute U X = Y
    # Outer loop, columns
for j in 1:m

    # Inner loop, rows, moving in reverse
    for i in 1:n

        ir       = n+1-i;

        XJ[ir,j] = @NLexpression(mldl, (YJ[ir,j] - sum(LUJ[ir,(n+1-k)]*XJ[(n+1-k),j] for k in 1:(i-1)))/LUJ[ir,ir] )

    end
end

# Retrieve values from model
Xm = getvalue(getindex(mldl, :XJ));

# Errors linear solves
errsol = norm(Xm-AD\B,2);
