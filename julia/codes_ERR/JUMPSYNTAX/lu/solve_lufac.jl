########################## solve_lufac #########################################
#
# Solve with the LU factorization of an 'invertible' matrix A = LU. The
# system to solve is
#
# A X = B, or LU X = B,
#
# where L is lower-triangular and U is upper-triangular.
#
# This function computes the solution X column-wise in two steps:
#
# 1. Solve for Y: L Y = B,
# 2. Solve for X: U X = Y.

# INPUT:
# L := Array (n x n), Unit lower triangular matrix,
# U := Array (n x n), Upper triangular matrix,
# B := Array (n x m), Right hand side.

# OUTPUT:
# X := Array (n x m), Solution.
#
################################################################################
# 06/12/19, J.B.

function solve_lufac(L,U,B)

    n = size(A,1);
    m = size(B,2);

    Y = zeros(n,m);
    X = zeros(n,m);

    # Step 1: Compute L Y = B
    # Outer loop, columns
    for j in 1:m

        # Inner loop, rows
        for i in 1:n

            Y[i,j] = B[i,j];

            # Element computations
            for k in 1:(i-1)

                Y[i,j] = Y[i,j] - (L[i,k]*Y[k,j]);

            end

            #L[i,j] = L[i,j]/U[j,j];

        end
    end

    # Step 2: Compute U X = Y
    # Outer loop, columns
    for j in 1:m

        # Inner loop, rows, moving in reverse
        for i in 1:n

            ir      = n+1-i;
            X[ir,j] = Y[ir,j];

            # Element computations
            for k in 1:(i-1)

                kr      = n+1-k;
                X[ir,j] = X[ir,j] - (U[ir,kr]*X[kr,j]);

            end

            X[ir,j] = X[ir,j]/U[ir,ir];

        end
    end

    return X
end
