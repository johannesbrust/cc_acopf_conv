########################## cholesky ############################################
#
# Cholesky factorization of a symmetric positive definite (spd) matrix A:
#
# A = R'R,
#
# where R is upper-triangular.
#
# This function forms the factorization column wise, and modifies the input
# matrix.
#
# INPUT:
# A := Array (n x n), Lower triangular part of a spd matrix,
#
# OUTPUT:
# A := Array (n x n), Lower triangular matrix, s.t. A on output corresponds to R'
#
################################################################################
# 05/30/19, J.B.

function cholesky(A)

    n = size(A,1);

    # Column loop
    for j in 1:n

        rjj = sqrt(A[j,j]);

        # Row loop
        for i in j:n

            # Inner column loop
            for k in 1:(j-1)

                A[i,j] = A[i,j] - (A[j,k]*A[i,k]);

            end

            A[i,j] = A[i,j]/rjj;

        end

    end

    return A;

end
