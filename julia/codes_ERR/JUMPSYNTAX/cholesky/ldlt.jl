########################## ldlt ################################################
#
# LDLT factorization of a symmetric matrix A:
#
# A = LDL',
#
# where L' is upper-triangular.
#
# This function forms the factorization column wise, and modifies the input
# matrix.
#
# INPUT:
# A := Array (n x n), Lower triangular part of a matrix,
#
# OUTPUTS:
# L := Array (n x n), Unit diagonal lower triangular matrix.
# D := Array (n x 1), Diagonal elements
#
################################################################################
# 05/30/19, J.B.

function ldlt(A)

    n = size(A,1);
    D = zeros(n,1);
    L = zeros(n,n);

    # Column loop
    for j in 1:n

        # D[j,1] = A[j,j];

        # Row loop
        for i in j:n

            L[i,j] = A[i,j];

            # Inner column loop
            for k in 1:(j-1)

                L[i,j] = L[i,j] - D[k,1]*(L[j,k]*L[i,k]);

                # if i == j
                #
                #     D[j,1] = D[j,1] - D[k,1]*(L[j,k]*L[i,k])
                #
                # end

            end

            if i == j

                D[j,1] = L[j,j];

            end

            L[i,j] = L[i,j]/D[j,1];

        end

    end

    return D, L;

end
