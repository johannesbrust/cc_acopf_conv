################################### arrays #####################################
#
# Script to form and overwrite arrays with JuMP nonlinear expressions
#
################################################################################
# 06/20/19, J.B.

#using Pkg
# This makes JuMP v0.19 available
using JuMP
using Ipopt
using LinearAlgebra

# Model
#mj      = Model(with_optimizer(Ipopt.Optimizer)); v19

mdl     = Model(solver=IpoptSolver(print_level=0));

@variable(mdl,x);

# Containers
n       = 10;
LUJ     = Array{Union{Float64,JuMP.NonlinearExpression},2}(undef, n, n);
In      = diagm(0=>ones(n));
zn      = zeros(n,n);

JuMP.registercon(mdl,:LUJ,LUJ);

for i in 1:n
    for j in 1:n

        if (i == j) && (i == 3)

            LUJ[i,j] = @NLexpression(mdl,sin(x));

        else

            LUJ[i,j] = @NLexpression(mdl,0);

        end

    end
end

TM      = [LUJ zn; zn In];

LUJ2     = Array{Union{Float64,JuMP.NonlinearExpression},2}(undef, 2*n, 2*n);
JuMP.registercon(mdl,:LUJ2,LUJ2);

for i in 1:2*n
    for j in 1:2*n

            # if (mod(i,n) != 0) & (mod(j,n) != 0)
            #
            #     LUJ2[i,j] = @NLexpression(mdl,(LUJ[mod(i,n),mod(j,n)])^2)
            #
            # end

            LUJ2[i,j] = @NLexpression(mdl,(TM[i,j])^2);

    end
end

setvalue(getindex(mdl, :x), 0.25*pi);
#setvalue(getindex(mldl, :AJ), AD);

LUJ2v = getvalue(getindex(mdl, :LUJ2));

# Test NLexpression.
#@NLexpression(m,rec[i=1:10,j=1:10], sum( 1 for k=1 if j==1 ) + sum( rec[i,k-1] for k=1:(j-1)) )
