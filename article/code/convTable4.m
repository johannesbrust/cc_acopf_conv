%----------------------------- convTable4.m ------------------------------%
%
% Table to generate convergence data and formats outputs to be
% LaTeX readable.
%
% This script uses a reduced set of data columns
%
% Additional "sub-rows" added for more detailed information, e.g.,
%   each case prints "sigma", "KP", "CONV" and "COND 30" outcomes
%
%-------------------------------------------------------------------------%
% 07/27/21, J.B., initial version
% 08/02/21, J.B., reduced table
% 12/07/21, J.B., Update to computation of values for table
%                   sigs = 1./CASES'.^2
%                   BE = (1./CASES'.^2).*KG.^2.*SA
%                   sigsUp = 1./(2.*KZ.*KG.*CASES'.*SA)*[1 10 100 ... 10^6]
% 12/08/21, J.B., Preparation of table for revision
% 12/13/21, J.B., Updated table for revision 2

% Sigma values (used in experiments)
% sigs = [...
%  0.0123457   0.123457     1.23457     12.3457       123.457        1234.57        12345.7;      
%  0.00111111  0.0111111    0.111111     1.11111       11.1111        111.111        1111.11;     
%  7.18184e-5  0.000718184  0.00718184   0.0718184      0.718184        7.18184        71.8184;   
%  1.11111e-5  0.000111111  0.00111111   0.0111111      0.111111        1.11111        11.1111;   
%  5.4546e-7   5.4546e-6    5.4546e-5    0.00054546     0.0054546       0.054546        0.54546;  
%  1.76097e-7  1.76097e-6   1.76097e-5   0.000176097    0.00176097      0.0176097       0.176097; 
%  1.2149e-7   1.2149e-6    1.2149e-5    0.00012149     0.0012149       0.012149        0.12149;  
%  1.17101e-8  1.17101e-7   1.17101e-6   1.17101e-5     0.000117101     0.00117101      0.0117101];

sigs =[...
   1.2346e-02   1.2346e-01   1.2346e+00   1.2346e+01   1.2346e+02   1.2346e+03   1.2346e+04   1.2346e+06
   1.1111e-03   1.1111e-02   1.1111e-01   1.1111e+00   1.1111e+01   1.1111e+02   1.1111e+03   1.1111e+05
   7.1818e-05   7.1818e-04   7.1818e-03   7.1818e-02   7.1818e-01   7.1818e+00   7.1818e+01   7.1818e+03
   1.1111e-05   1.1111e-04   1.1111e-03   1.1111e-02   1.1111e-01   1.1111e+00   1.1111e+01   1.1111e+03
   5.4546e-07   5.4546e-06   5.4546e-05   5.4546e-04   5.4546e-03   5.4546e-02   5.4546e-01   5.4546e+01
   1.7610e-07   1.7610e-06   1.7610e-05   1.7610e-04   1.7610e-03   1.7610e-02   1.7610e-01   1.7610e+01
   1.2149e-07   1.2149e-06   1.2149e-05   1.2149e-04   1.2149e-03   1.2149e-02   1.2149e-01   1.2149e+01
   1.1710e-08   1.1710e-07   1.1710e-06   1.1710e-05   1.1710e-04   1.1710e-03   1.1710e-02   1.1710e+00];

% sigsUp = [...
%     0.01359       0.1359        1.359        13.59        135.9         1359        13590
%     0.0015293     0.015293      0.15293       1.5293       15.293       152.93       1529.3
%    3.2913e-05   0.00032913    0.0032913     0.032913      0.32913       3.2913       32.913
%    1.7901e-06   1.7901e-05   0.00017901    0.0017901     0.017901      0.17901       1.7901
%    6.2718e-07   6.2718e-06   6.2718e-05   0.00062718    0.0062718     0.062718      0.62718
%    1.4275e-08   1.4275e-07   1.4275e-06   1.4275e-05   0.00014275    0.0014275     0.014275
%    1.2498e-07   1.2498e-06   1.2498e-05   0.00012498    0.0012498     0.012498      0.12498
%    4.5593e-09   4.5593e-08   4.5593e-07   4.5593e-06   4.5593e-05   0.00045593    0.0045593];

sigsUp = [...
   1.3590e-02   1.3590e-01   1.3590e+00   1.3590e+01   1.3590e+02   1.3590e+03   1.3590e+04   1.3590e+06
   1.5293e-03   1.5293e-02   1.5293e-01   1.5293e+00   1.5293e+01   1.5293e+02   1.5293e+03   1.5293e+05
   3.2913e-05   3.2913e-04   3.2913e-03   3.2913e-02   3.2913e-01   3.2913e+00   3.2913e+01   3.2913e+03
   1.7901e-06   1.7901e-05   1.7901e-04   1.7901e-03   1.7901e-02   1.7901e-01   1.7901e+00   1.7901e+02
   6.2718e-07   6.2718e-06   6.2718e-05   6.2718e-04   6.2718e-03   6.2718e-02   6.2718e-01   6.2718e+01
   1.4275e-08   1.4275e-07   1.4275e-06   1.4275e-05   1.4275e-04   1.4275e-03   1.4275e-02   1.4275e+00
   1.2498e-07   1.2498e-06   1.2498e-05   1.2498e-04   1.2498e-03   1.2498e-02   1.2498e-01   1.2498e+01
   4.5593e-09   4.5593e-08   4.5593e-07   4.5593e-06   4.5593e-05   4.5593e-04   4.5593e-03   4.5593e-01];

% Indices for sigma values
%inds = [1 2 3 4 5 7]; % original
%inds = [1 2 5 7]; % fewer columns

inds = [1 2 5 7 8]; % fewer columns

% Bound estimate
% BE = [...
%  0.0628117    0.628117     6.28117     62.8117     628.117     6281.17    62811.7;      
%  0.0803474    0.803474     8.03474     80.3474     803.474     8034.74    80347.4;      
%  0.0452983    0.452983     4.52983     45.2983     452.983     4529.83    45298.3;      
%  0.189169     1.89169     18.9169     189.169     1891.69     18916.9         1.89169e5;
%  0.00127926   0.0127926    0.127926     1.27926     12.7926     127.926    1279.26;     
%  0.0915551    0.915551     9.15551     91.5551     915.551     9155.51    91555.1;      
%  0.000777488  0.00777488   0.0777488    0.777488     7.77488     77.7488    777.488;    
%  0.00196507   0.0196507    0.196507     1.96507     19.6507     196.507    1965.07];

BE = [...
   6.2812e-02   6.2812e-01   6.2812e+00   6.2812e+01   6.2812e+02   6.2812e+03   6.2812e+04   6.2812e+06
   8.0347e-02   8.0347e-01   8.0347e+00   8.0347e+01   8.0347e+02   8.0347e+03   8.0347e+04   8.0347e+06
   4.5298e-02   4.5298e-01   4.5298e+00   4.5298e+01   4.5298e+02   4.5298e+03   4.5298e+04   4.5298e+06
   1.8917e-01   1.8917e+00   1.8917e+01   1.8917e+02   1.8917e+03   1.8917e+04   1.8917e+05   1.8917e+07
   1.2793e-03   1.2793e-02   1.2793e-01   1.2793e+00   1.2793e+01   1.2793e+02   1.2793e+03   1.2793e+05
   9.1555e-02   9.1555e-01   9.1555e+00   9.1555e+01   9.1555e+02   9.1555e+03   9.1555e+04   9.1555e+06
   7.7749e-04   7.7749e-03   7.7749e-02   7.7749e-01   7.7749e+00   7.7749e+01   7.7749e+02   7.7749e+04
   1.9651e-03   1.9651e-02   1.9651e-01   1.9651e+00   1.9651e+01   1.9651e+02   1.9651e+03   1.9651e+05];

% Updated bound estimate
% BEUp=[0.069143      0.69143       6.9143       69.143       691.43       6914.3        69143
%       0.11059       1.1059       11.059       110.59       1105.9        11059   1.1059e+05
%      0.020759      0.20759       2.0759       20.759       207.59       2075.9        20759
%      0.030477      0.30477       3.0477       30.477       304.77       3047.7        30477
%     0.0014709     0.014709      0.14709       1.4709       14.709       147.09       1470.9
%     0.0074218     0.074218      0.74218       7.4218       74.218       742.18       7421.8
%    0.00079983    0.0079983     0.079983      0.79983       7.9983       79.983       799.83
%    0.00076509    0.0076509     0.076509      0.76509       7.6509       76.509       765.09];

BEUp = [...
   6.9143e-02   6.9143e-01   6.9143e+00   6.9143e+01   6.9143e+02   6.9143e+03   6.9143e+04   6.9143e+06
   1.1059e-01   1.1059e+00   1.1059e+01   1.1059e+02   1.1059e+03   1.1059e+04   1.1059e+05   1.1059e+07
   2.0759e-02   2.0759e-01   2.0759e+00   2.0759e+01   2.0759e+02   2.0759e+03   2.0759e+04   2.0759e+06
   3.0477e-02   3.0477e-01   3.0477e+00   3.0477e+01   3.0477e+02   3.0477e+03   3.0477e+04   3.0477e+06
   1.4709e-03   1.4709e-02   1.4709e-01   1.4709e+00   1.4709e+01   1.4709e+02   1.4709e+03   1.4709e+05
   7.4218e-03   7.4218e-02   7.4218e-01   7.4218e+00   7.4218e+01   7.4218e+02   7.4218e+03   7.4218e+05
   7.9983e-04   7.9983e-03   7.9983e-02   7.9983e-01   7.9983e+00   7.9983e+01   7.9983e+02   7.9983e+04
   7.6509e-04   7.6509e-03   7.6509e-02   7.6509e-01   7.6509e+00   7.6509e+01   7.6509e+02   7.6509e+04];

% BB2 =[...
%    2.1735e-03   2.1735e-02   2.1735e-01   2.1735e+00   2.1735e+01   2.1735e+02   2.1735e+03   2.1735e+05
%    9.2676e-03   9.2676e-02   9.2676e-01   9.2676e+00   9.2676e+01   9.2676e+02   9.2676e+03   9.2676e+05
%    2.0551e-02   2.0551e-01   2.0551e+00   2.0551e+01   2.0551e+02   2.0551e+03   2.0551e+04   2.0551e+06
%    2.1820e-01   2.1820e+00   2.1820e+01   2.1820e+02   2.1820e+03   2.1820e+04   2.1820e+05   2.1820e+07
%    6.6596e-03   6.6596e-02   6.6596e-01   6.6596e+00   6.6596e+01   6.6596e+02   6.6596e+03   6.6596e+05
%    8.3884e-01   8.3884e+00   8.3884e+01   8.3884e+02   8.3884e+03   8.3884e+04   8.3884e+05   8.3884e+07
%    8.5763e-03   8.5763e-02   8.5763e-01   8.5763e+00   8.5763e+01   8.5763e+02   8.5763e+03   8.5763e+05
%    6.9819e-02   6.9819e-01   6.9819e+00   6.9819e+01   6.9819e+02   6.9819e+03   6.9819e+04   6.9819e+06];

[nrows,ncols] = size(sigs(:,inds));

% Full convergence values
% CONVS = [...
%     true    
% true
% true
% true
% true
% true    
% true 
% true   
% true
% false
% true
% false
% true
% true
% true
% true
% true
% false
% true
% false
% true
% true
% true
% true
% true
% false
% true
% false
% true
% true
% true
% true
% true
% false
% false
% false
% true
% true
% true
% true
% false
% false
% true
% false
% true
% true
% true
% true
% false
% false
% false
% false
% true
% false
% true
% false];

CONVS = [...
    true    
true
true
true
true
true    
true 
true   
true
false
true
false
true
true
true
true
true
false
true
false
true
true
true
true
true
false
true
false
true
true
true
true
true
false
false
false
true
true
true
true
false
false
true
false
true
true
true
true
false
false
false
false
true
false
true
false
false
false
false
false
false
false
false
false];

CONVS_R = reshape(CONVS,nrows,size(sigs,2));

CASES = [9  30  118  300  1354  2383  2869  9241];
SA = [2     1    16    31    90   253   185   511];
KZ = 1.2816;

%KX = 0.1;
%KX=0.0015;
%KX = [0.1, 0.1, 0.01, 0.001, 0.1, 0.001, 0.1, 0.01];
KX = [0.005, 0.1, 0.005, 0.005, 0.02, 0.001, 0.01, 0.002];
KGAMS = [1.5949e+00   8.5037e+00   6.2786e+00   2.3435e+01   5.1049e+00   4.5332e+01   5.8815e+00   1.8122e+01];

% Recompute estimate
BB2 = 2.*KZ.*BE.*(repmat((CASES.*KX)',1,size(BE,2)));

% Container
subrows = 4;
table = cell(nrows*subrows,1);

% Generate table
for i=1:nrows
    
    for k=1:subrows
        
        if (i==1) && (k==1)
            tfrmt = '\\multirow{%i}{*}{$%.2g$} &';
            tblr = sprintf(tfrmt,nrows*subrows,KZ);
        else
            tblr = '&';
        end
        
        if k==1
            rv = sprintf('\\multirow{%i}{*}{$%i$} &',subrows,CASES(i));
            tblr = [tblr,rv]; %#ok<AGROW> 
            % KX
            tfrmt = '\\multirow{%i}{*}{$%.2g$} &';
            rv = sprintf(tfrmt,subrows,KX(i));
            tblr = [tblr,rv]; %#ok<AGROW> 
            % KG
            tfrmt = '\\multirow{%i}{*}{$%.2g$} &';
            rv = sprintf(tfrmt,subrows,KGAMS(i));
            tblr = [tblr,rv]; %#ok<AGROW> 
            % NA
            tfrmt = '\\multirow{%i}{*}{$%i$} &';
            rv = sprintf(tfrmt,subrows,SA(i));
            tblr = [tblr,rv]; %#ok<AGROW> 
        else
            tblr = [tblr,' & & & & ']; %#ok<AGROW>
        end
        
        % Printing subrows
        if k==1
            tblr = [tblr,' $\sigma$ & ']; %#ok<AGROW> 
        elseif k==2
            tblr = [tblr,' $K_{P}$ & ']; %#ok<AGROW> \Gamma
        elseif k==3
            tblr = [tblr,' $\texttt{Conv.}$ & ']; %#ok<AGROW> 
        elseif k==4
            tblr = [tblr,' $\texttt{Cond. 30}$ & ']; %#ok<AGROW> 
        end
        
        %tblr = sprintf('$ %i $ &',CASES(i));
        
        for j=1:ncols
            
            lastc = '&';
            if j == ncols
                lastc ='\\';
                if k==4
                    if i==nrows
                        lastc = [lastc,'\cline{1-11}']; %#ok<AGROW> 
                    else
                        lastc = [lastc,'\cline{2-11}']; %#ok<AGROW> 
                    end
                end
            end
            
            conv = '{\boxed{\texttt{Y}} }'; % \scriptsize
            if CONVS_R(i,inds(j)) == 0
                conv = '{ \texttt{N}}'; % \scriptsize
            end
            
%             cond30 = '{\texttt{Y}}'; % \scriptsize
%             if BB2(i,inds(j)) >= 1
%                 cond30 = '{ \texttt{N}}'; % \scriptsize
%             end
            
            tfrmt = '%.2g';
            cond30 = sprintf(tfrmt,BB2(i,inds(j)));
            
            tfrmt = '%.2g';
            sv = sprintf(tfrmt,sigs(i,inds(j)));
            
            bv = sprintf(tfrmt,BE(i,inds(j)));
            
             
            
            sBE = sort(BE(:,inds(j))); % ,'ASCEND'
            
            % Adjust highlighting
            if BE(i,inds(j)) <= sBE(1)
                
                bv = ['\textbf{',bv,'}']; %#ok<AGROW>
                
            elseif BE(i,inds(j)) <= sBE(2)
                
                bv = ['\emph{',bv,'}']; %#ok<AGROW>
                
            end
            
            % Printing subrows
            if k==1
                rv = sprintf('$\\texttt{%s}$',sv);
                %tblr = [tblr,' $\sigma$ & ']; %#ok<AGROW> 
            elseif k==2
                rv = sprintf('$\\texttt{%s}$',bv);
            elseif k==3
                rv = sprintf('$%s$',conv);
            elseif k==4
                rv = sprintf('$\\texttt{%s}$',cond30);
            end
            
%             rv = sprintf(['$ {\\scriptsize \\underset{{\\scriptsize \\texttt{%s}}}',...
%                 '{\\begin{array}{c} \\texttt{%s} \\\\ \\texttt{%s} \\end{array}}} $'],...
%                 conv,sv,bv);
            
            tblr = [tblr,rv,lastc]; %#ok<AGROW>
            
        end
        
        table{(i-1)*subrows+k} = tblr;
        
    end
end

% Print to file
fid = fopen('table4.txt','w');
fprintf(fid,'%s \n',table{:});
fclose(fid);

