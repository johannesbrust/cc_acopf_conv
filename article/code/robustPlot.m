%--------------------------- robustPlot ----------------------------------%
%
% Plot for comparing outcomes when system parameters are changed,
% i.e., percentage range for demands
%
%
%-------------------------------------------------------------------------%
% 07/30/21, J.B., Initial implementation

figname = 'PerturbedProbs';
figPath = '.';

x0 = 0; y0 = 0;
width =4; height =4;

figure('Units','inches',...
    'Position',[x0 y0 width height],...
    'PaperPositionMode','auto');

% ticks: 0.95:0.01:1.05
D1 = [...
    4916.512773324971	4991.3357922516525	5066.888318012173	5143.17087671871	5220.18399569579	5297.928203620152	5376.404030635772	5455.612007902559	5535.552668282797	5616.226545889131	5697.634176096439
    4916.512773324971	4991.3357922516525	5066.888318012173	5143.17087671871	5220.18399569579	5297.928203620152	5376.404030635772	5455.612007902559	5535.552668282797	5616.226545889131	5697.634176096439
    538.341013218099	545.9093400922433	553.6051553650623	561.4374678749407	569.4523386209612	578.008166900089	589.6255343979839	612.1875450802994	622.464760588384	0.0	0.0
    121285.32319586037	122955.764986309	124628.7314290818	126304.15272493723	127981.92617916546	129662.05355833906	131344.52985908405	133029.29874304633	134716.34657548767	136405.6312694234	138097.1003683666
    671948.4857920562	681425.7050380685	690952.8665617867	700544.5355904055	710221.8868735166	720090.3315447781	730553.2925980719	743931.5315993609	754243.7022186774	790923.6349728071	770081.4817220857
    70307.83046045901	71059.26516459133	71811.08346046893	72563.38521628422	73316.16756319959	74069.38397267048	74823.0339311036	75577.11692085295	76331.66711698714	77086.71351163632	77842.26543285421
    1.6849582309647202e6	1.7201645382076714e6	1.755791536288734e6	1.7921297216523492e6	1.8291485484838528e6	1.8685512505766354e6	1.9089767962405665e6	0.0	0.0	0.0	0.0
    127210.17282045624	128566.75390800703	129923.94585829493	131281.76466214223	132640.21737320576	133999.3083487583	135359.06911408677	136719.66380271397	138081.66280102317	139444.90320990083	140809.55351124404
    299920.68068523	303115.4379373216	306311.89166306664	309510.1692166377	312710.44727290195	315912.67956398235	319116.92429454037	322323.6420792551	325537.50707238197	328995.7801665075	0.0];

% Convergence flags for above experiments
CONVS1 =[...
    true	true	true	true	true	true	true	true	true	true	true
    true	true	true	true	true	true	true	true	true	true	true
    true	true	true	true	true	true	true	true	false	false	false
    true	true	true	true	true	true	true	true	true	true	true
    true	true	true	true	true	true	true	true	false	false	false
    true	true	true	true	true	true	true	true	true	true	true
    true	true	true	true	true	true	true	false	false	false	false
    true	true	true	true	true	true	true	true	true	true	true
    true	true	true	true	true	true	true	true	true	false	false];

CONVS1DET = [...
    true	true	true	true	true	true	true	true	true	true	true
    true	true	true	true	true	true	true	true	true	true	true
    true	true	true	true	true	true	true	true	true	false	false
    true	true	true	true	true	true	true	true	true	true	true
    true	true	true	true	true	true	true	true	true	true	true
    true	true	true	true	true	true	true	true	true	true	true
    true	true	true	true	true	true	true	false	false	false	false
    true	true	true	true	true	true	true	true	true	true	true
    true	true	true	true	true	true	true	true	true	false	false];

% D1 selected for "converged" experiments
D1SEL = D1.*CONVS1;

% ticks 0.8:0.05:1.2
D2 = [...
    3881.354729907824	4208.290706303197	4553.3219681349865	4916.512773324971	5297.928203620152	5697.634176096439	6115.7059665186625	6552.252740887085	7007.359706998443
    3881.354729907824	4208.290706303197	4553.3219681349865	4916.512773324971	5297.928203620152	5697.634176096439	6115.7059665186625	6552.252740887085	7007.359706998443
    432.11754634255567	466.7868702508025	502.0957649816706	538.341013218099	578.008166900089	0.0	0.0	0.0	0.0
    96727.88612532541	104764.54086174481	112974.12271958566	121285.32319586037	129662.05355833906	138097.1003683666	146586.19105723235	155127.33574936725	163719.46424504244
    535445.7230424874	579713.0789439448	625258.3477000184	671948.4857920562	720090.3315447781	770081.4817220857	0.0	0.0	0.0
    59079.03674979042	62813.69801767503	66556.47657642594	70307.83046045901	74069.38397267048	77842.26543285421	81628.75224427096	0.0	0.0
    1.1817152603987078e6	1.3437562947713172e6	1.513087302695001e6	1.6849582309647202e6	1.8685512505766354e6	0.0	0.0	0.0	0.0
    106932.0182253932	113677.26190741535	120436.12669433551	127210.17282045624	133999.3083487583	140809.55351124404	0.0	0.0	0.0
    252168.94145453293	268053.9121984565	283970.02743044676	299920.68068523	315912.67956398235	0.0	0.0	0.0	0.0];

% Convergence flags
CONVS2 = [...
    true	true	true	true	true	true	true	true	true
    true	true	true	true	true	true	true	true	true
    true	true	true	true	true	false	false	false	false
    true	true	true	true	true	true	true	true	true
    true	true	true	true	true	false	false	false	false
    true	true	true	true	true	true	true	false	false
    true	true	true	true	true	false	false	false	false
    true	true	true	true	true	true	false	false	false
    true	true	true	true	true	false	false	false	false];

% D2 selected for "converged"
D2SEL = D2.*CONVS2;

% Merging array (and removing 1st index)
cols1 = 1:4;
cols2 = 6:9;
cols3 = 2:10;
rows = 2:9;
DMERGED = [D2SEL(rows,cols1) D1SEL(rows,cols3) D2SEL(rows,cols2) ];

% X values
int1 = 0.8:0.05:0.95;
int2 = 0.96:0.01:1.04;
int3 = 1.05:0.05:1.2;

xVals = [int1,int2,int3];

% Rescale rows
medIdx = median(1:length(xVals));
dScale = diag(DMERGED(:,medIdx));

% Scaling
SDMERGED = dScale \ DMERGED;

% plots
fnts = 12;

%markers = [ ':s' ':o' ':^' ':v' ':p' ':<' ':x' ':h' ':+' ':d' ':*' ':<' ];

markers = { 'bs:', 'ro:', 'g^:', 'kv:', 'cp:', 'm<:', 'yx:', 'bh:', 'r+:', 'gd:', 'k*:', 'c<:'};

xlabel('Load (normalized)','Interpreter','Latex','FontSize',fnts+2);
ylabel('$\textnormal{Objective (normalized)}$',...
    'Interpreter','Latex','FontSize',fnts+2);
title('Perturbed Problems','Interpreter','Latex','FontSize',fnts+2)

hold on;
nprobs = size(SDMERGED,1);
% Plot lines one-by-one
for i = 1:nprobs
    %plot(xVals,SDMERGED(i,:),markers(i));
    plot(xVals,SDMERGED(i,:),markers{i});
end
%plot(xVals,SDMERGED,':o');
%plot(xVals,SDMERGED,markers(2:9));



% Selecting subsets
%cols4 = 5:13;
cols4 = 4:14;
xValsSEL = xVals(cols4);
XValsSEL = repmat(xValsSEL,length(rows));

SDMERGEDSEL = SDMERGED(:,cols4);

% Add square (?) for non-converged problems
NCONVS = CONVS1(rows,:) ~= CONVS1DET(rows,:);

% additional points to mark nonconvergent problems
%plot(XValsSEL(NCONVS(cols3)),SDMERGEDSEL(NCONVS(cols3)),'s');
plot(XValsSEL(NCONVS),SDMERGEDSEL(NCONVS),'s','MarkerSize',12,...
    'MarkerEdgeColor','black','MarkerFaceColor','none'); % [0.8 0.8 0.8]

cases={...
    'IEEE 9';
    'IEEE 30';
    'IEEE 118';
    'IEEE 300';
    'IEEE 1354';
    'IEEE 2383';
    'IEEE 2869';
    'IEEE 9241'};

% horizontal line
line([xVals(1); xVals(end)],[1 1],'color',[0.1 0.1 0.1], 'LineWidth',0.5,'LineStyle','--');

legend(cases,'Interpreter','Latex','Location','SouthWest','FontSize',0.8*fnts);

grid on;

box on;

ylimLOC = ylim;

% Set modified y-limit
ylim([-0.1 ylimLOC(2)]);

hold off;

fig                     = gcf;
fig.PaperPositionMode   = 'auto';
fig_pos                 = fig.PaperPosition;
fig.PaperSize           = [fig_pos(3) fig_pos(4)];

figname = [figname,'.pdf'];

print(fig,'-dpdf',fullfile(figPath,figname));