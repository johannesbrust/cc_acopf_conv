%------------------------ EXPERIMENT I -----------------------------------%
%
% Script to generate plots for the chance-constrained manuscript.
% The solver is implemented in Julia an is in
% /Users/johannesbrust/Dropbox/ANL/projects/LOAD_DYNAMIC/code/chanceconstrainedacopf
%
% Data for plots comes from the Julia scripts:
% /Users/johannesbrust/Dropbox/ANL/students/EGRIMM/project/codes/chanceconstrainedacopf
%-------------------------------------------------------------------------%
% 02/20/20, J.B.
% 12/18/20, J.B., Modification of labeling

colors  = ['b' 'r' 'k' 'm' 'c' 'g' 'y'];   
lines   = {'-' '--' '-.'};
markers = [ 's' 'o' '^' 'v' 'p' '<' 'x' 'h' '+' 'd' '*' '<' ];

leglocation = 'NorthWest';
figPath = '.';

isbeam = 1;
legstr = {'CC-ACOPF:Alg.1',...
            'CC-ACOPF'};
figname = 'EXPERIMENT_I';

stitle  = 'IEEE 30:';
        
if isbeam == 1
    x0 = 0; y0 = 0;
    width =4; height =3;

    figure('Units','inches',...
        'Position',[x0 y0 width height],...
        'PaperPositionMode','auto');
else
    figure;
end

% Data from Julia experiments
C = [578.228 578.064 577.935 577.83 577.742 577.667 577.602 577.545...
    577.494 577.449 577.409 577.373 577.34 577.31 577.282 577.257];

B = [578.207 578.047 577.921 577.818 577.733 577.659 577.596 577.54...
    577.49 577.446 577.406 577.37 577.338 577.308 577.281 577.256];

ys = [C',B'];
xs = [1-(0.05:0.01:0.2)]';

%plot(xs,ys,['bo-.','ro-.'],'LineWidth',2);

plot(xs,ys(:,1),'b+-',xs,ys(:,2),'ro-.','LineWidth',2);

grid on;

legend(legstr,'Location',leglocation,'Fontsize',14,'interpreter','latex');
xlabel('$1-\epsilon_v$','FontSize',14','interpreter','latex');
ylabel('$C(\mathbf{s}^*)$','FontSize',14,'interpreter','latex');
title(stitle,'interpreter','latex');     

fig                     = gcf;
fig.PaperPositionMode   = 'auto';
fig_pos                 = fig.PaperPosition;
fig.PaperSize           = [fig_pos(3) fig_pos(4)];

figname = [figname,'.pdf'];

print(fig,'-dpdf',fullfile(figPath,figname));