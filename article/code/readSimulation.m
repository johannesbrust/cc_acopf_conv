%-------------------------- readSimulation -------------------------------%
%
% Script to read the simulation data from the CASE 9 experiment
% with random load realizations
%
% This script uses the file "data_EX3_SIM_JOINT_CONS.txt"
% 
%-------------------------------------------------------------------------%
% 08/02/21, J.B., Implementation of file input
% 08/03/21, J.B., Preparation of output figures

fid = fopen('data_EX3_SIM_JOINT_CONS.txt');

readRes = textscan(fid,'%f');

fntsize = 12; 

% Figure names/description
figname = 'hist1';
figPath = '.';

x0 = 0; y0 = 0;
width =4; height =4;

figure('Units','inches',...
    'Position',[x0 y0 width height],...
    'PaperPositionMode','auto');


% This problem has 9 variables of which 1,2,3 correspond to generators
% (The remaining variables are load busses)
%
% The simulation were done 500 times

resStacked = readRes{:};

% Reshape results
nvars = 9;
nsim = 500;
vms = reshape(resStacked,nsim,nvars);

% Bound value 1.1
bnd = 1.1;
dist1 = sum(vms<=bnd,2);

% Plot the histogram
h1 = histogram(dist1,'FaceColor','blue','Normalization','probability');
title('$\textnormal{Joint Frequencies}$','Interpreter','Latex','Fontsize',fntsize);

ylabel('$ P(\mathbf{v} \le \mathbf{u}) $','Interpreter','Latex',...
    'Fontsize',fntsize);

xlabel('$ \mathbf{v} \le \mathbf{u} $','Interpreter','Latex',...
    'Fontsize',fntsize);

% \textnormal{count}( )

% Print 1st distribution

fig                     = gcf;
fig.PaperPositionMode   = 'auto';
fig_pos                 = fig.PaperPosition;
fig.PaperSize           = [fig_pos(3) fig_pos(4)];

figname = [figname,'.pdf'];

print(fig,'-dpdf',fullfile(figPath,figname));

% Second distribution
dist2 = sum(vms<=bnd,1);

figure('Units','inches',...
    'Position',[x0 y0 width height],...
    'PaperPositionMode','auto');

%histogram(dist2);
b1 = bar((dist2 ./500),'hist');
set(b1,'FaceAlpha',0.6);
set(b1,'FaceColor','blue');

ylim([0 1.1]);

title('$\textnormal{Disjoint Frequencies}$','Interpreter','Latex','Fontsize',fntsize);

ylabel('$ P( v_i \le u_i) $','Interpreter','Latex',...
    'Fontsize',fntsize);

xlabel('$ v_i $','Interpreter','Latex',...
    'Fontsize',fntsize);


% Print 2nd distribution

figname = 'hist2';

fig                     = gcf;
fig.PaperPositionMode   = 'auto';
fig_pos                 = fig.PaperPosition;
fig.PaperSize           = [fig_pos(3) fig_pos(4)];

figname = [figname,'.pdf'];

print(fig,'-dpdf',fullfile(figPath,figname));