%------------------------ EXPERIMENT Ia ----------------------------------%
%
% Script to generate plots for the chance-constrained manuscript.
% The solver is implemented in Julia an is in
% /Users/johannesbrust/Dropbox/ANL/projects/LOAD_DYNAMIC/code/chanceconstrainedacopf
%
% Data for plots comes from the Julia scripts:
% /Users/johannesbrust/Dropbox/ANL/students/EGRIMM/project/codes/chanceconstrainedacopf
% This uses a 9 Bus test case.
%-------------------------------------------------------------------------%
% 02/20/20, J.B.
% 12/18/20, J.B., Modification of labeling

colors  = ['b' 'r' 'k' 'm' 'c' 'g' 'y'];   
lines   = {'-' '--' '-.'};
markers = [ 's' 'o' '^' 'v' 'p' '<' 'x' 'h' '+' 'd' '*' '<' ];

leglocation = 'NorthWest';
figPath = '.';

isbeam = 1;
legstr = {'CC-ACOPF:Alg.1',...
            'CC-ACOPF'};
figname = 'EXPERIMENT_Ia';

stitle  = 'IEEE 9:';
        
if isbeam == 1
    x0 = 0; y0 = 0;
    width =4; height =3;

    figure('Units','inches',...
        'Position',[x0 y0 width height],...
        'PaperPositionMode','auto');
else
    figure;
end

% Data from Julia experiments
ys = [5298.292402831254   5298.292399741962;
 5298.201692764205   5298.201689871499; 
 5298.122379697258   5298.12237700544;  
 5298.051539384691   5298.0515369854875;
 5297.987256231878   5297.987254001286; 
 5297.928203620151   5297.928201430894; 
 5297.873423305765   5297.873421254069; 
 5297.822199516627   5297.822197619587; 
 5297.773982373757   5297.773980608061; 
 5297.728340280789   5297.728338638581; 
 5297.684927597549   5297.684926071512; 
 5297.6434630102285  5297.643461593576; 
 5297.603714273563   5297.603713039079; 
 5297.565487240788   5297.565486103575; 
 5297.528617807635   5297.528616762339; 
 5297.492965886032   5297.492964927082;];

xs = [1-(0.05:0.01:0.2)]';

%plot(xs,ys,['bo-.','ro-.'],'LineWidth',2);

plot(xs,ys(:,1),'b+-',xs,ys(:,2),'ro-.','LineWidth',2);

grid on;

legend(legstr,'Location',leglocation,'Fontsize',14,'interpreter','latex');
xlabel('$1-\epsilon_v$','FontSize',14','interpreter','latex');
ylabel('$C(\mathbf{s}^*)$','FontSize',14,'interpreter','latex');
title(stitle,'interpreter','latex');     

fig                     = gcf;
fig.PaperPositionMode   = 'auto';
fig_pos                 = fig.PaperPosition;
fig.PaperSize           = [fig_pos(3) fig_pos(4)];

figname = [figname,'.pdf'];

print(fig,'-dpdf',fullfile(figPath,figname));