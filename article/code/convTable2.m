%----------------------------- convTable1.m ------------------------------%
%
% Table to generate convergence data and formats outputs to be
% LaTeX readable.
%
% This script uses a reduced set of data columns
%
%-------------------------------------------------------------------------%
% 07/27/21, J.B., initial version
% 08/02/21, J.B., reduced table
% 12/07/21, J.B., Update to computation of values for table
%                   sigs = 1./CASES'.^2
%                   BE = (1./CASES'.^2).*KG.^2.*SA
%                   sigsUp = 1./(2.*KZ.*KG.*CASES'.*SA)*[1 10 100 ... 10^6]

% Sigma values (used in experiments)
sigs = [...
 0.0123457   0.123457     1.23457     12.3457       123.457        1234.57        12345.7;      
 0.00111111  0.0111111    0.111111     1.11111       11.1111        111.111        1111.11;     
 7.18184e-5  0.000718184  0.00718184   0.0718184      0.718184        7.18184        71.8184;   
 1.11111e-5  0.000111111  0.00111111   0.0111111      0.111111        1.11111        11.1111;   
 5.4546e-7   5.4546e-6    5.4546e-5    0.00054546     0.0054546       0.054546        0.54546;  
 1.76097e-7  1.76097e-6   1.76097e-5   0.000176097    0.00176097      0.0176097       0.176097; 
 1.2149e-7   1.2149e-6    1.2149e-5    0.00012149     0.0012149       0.012149        0.12149;  
 1.17101e-8  1.17101e-7   1.17101e-6   1.17101e-5     0.000117101     0.00117101      0.0117101];

sigsUp = [...
    0.01359       0.1359        1.359        13.59        135.9         1359        13590
    0.0015293     0.015293      0.15293       1.5293       15.293       152.93       1529.3
   3.2913e-05   0.00032913    0.0032913     0.032913      0.32913       3.2913       32.913
   1.7901e-06   1.7901e-05   0.00017901    0.0017901     0.017901      0.17901       1.7901
   6.2718e-07   6.2718e-06   6.2718e-05   0.00062718    0.0062718     0.062718      0.62718
   1.4275e-08   1.4275e-07   1.4275e-06   1.4275e-05   0.00014275    0.0014275     0.014275
   1.2498e-07   1.2498e-06   1.2498e-05   0.00012498    0.0012498     0.012498      0.12498
   4.5593e-09   4.5593e-08   4.5593e-07   4.5593e-06   4.5593e-05   0.00045593    0.0045593];

% Indices for sigma values
%inds = [1 2 3 4 5 7]; % original
inds = [1 2 5 7]; % fewer columns

% Bound estimate
BE = [...
 0.0628117    0.628117     6.28117     62.8117     628.117     6281.17    62811.7;      
 0.0803474    0.803474     8.03474     80.3474     803.474     8034.74    80347.4;      
 0.0452983    0.452983     4.52983     45.2983     452.983     4529.83    45298.3;      
 0.189169     1.89169     18.9169     189.169     1891.69     18916.9         1.89169e5;
 0.00127926   0.0127926    0.127926     1.27926     12.7926     127.926    1279.26;     
 0.0915551    0.915551     9.15551     91.5551     915.551     9155.51    91555.1;      
 0.000777488  0.00777488   0.0777488    0.777488     7.77488     77.7488    777.488;    
 0.00196507   0.0196507    0.196507     1.96507     19.6507     196.507    1965.07];

% Updated bound estimate
BEUp=[0.069143      0.69143       6.9143       69.143       691.43       6914.3        69143
      0.11059       1.1059       11.059       110.59       1105.9        11059   1.1059e+05
     0.020759      0.20759       2.0759       20.759       207.59       2075.9        20759
     0.030477      0.30477       3.0477       30.477       304.77       3047.7        30477
    0.0014709     0.014709      0.14709       1.4709       14.709       147.09       1470.9
    0.0074218     0.074218      0.74218       7.4218       74.218       742.18       7421.8
   0.00079983    0.0079983     0.079983      0.79983       7.9983       79.983       799.83
   0.00076509    0.0076509     0.076509      0.76509       7.6509       76.509       765.09];

[nrows,ncols] = size(sigs(:,inds));

% Full convergence values
CONVS = [...
    true    
true
true
true
true
true    
true 
true   
true
false
true
false
true
true
true
true
true
false
true
false
true
true
true
true
true
false
true
false
true
true
true
true
true
false
false
false
true
true
true
true
false
false
true
false
true
true
true
true
false
false
false
false
true
false
true
false];

CONVS_R = reshape(CONVS,nrows,size(sigs,2));

CASES = [9  30  118  300  1354  2383  2869  9241];

SA = [2     1    16    31    90   253   185   511];
KZ = 1.2816;

% Container
table = cell(nrows,1);

% Generate table
for i=1:nrows
    
    tblr = sprintf('$ %i $ &',CASES(i));
    
    for j=1:ncols
        
        lastc = '&';
        if j == ncols
            lastc ='\\';
        end
        
        conv = '{\scriptsize \boxed{\texttt{Y}} }'; % 
        if CONVS_R(i,inds(j)) == 0
            conv = '{\scriptsize \texttt{N}}';
        end
        
        tfrmt = '%.2g';
        sv = sprintf(tfrmt,sigs(i,inds(j)));
        
        bv = sprintf(tfrmt,BE(i,inds(j)));
        
        sBE = sort(BE(:,inds(j))); % ,'ASCEND'
        
        % Adjust highlighting
        if BE(i,inds(j)) <= sBE(1)
            
            bv = ['\textbf{',bv,'}']; %#ok<AGROW>
            
        elseif BE(i,inds(j)) <= sBE(2)
            
            bv = ['\emph{',bv,'}']; %#ok<AGROW>
            
        end
        
        rv = sprintf(['$ {\\scriptsize \\underset{{\\scriptsize \\texttt{%s}}}',...
            '{\\begin{array}{c} \\texttt{%s} \\\\ \\texttt{%s} \\end{array}}} $'],...
            conv,sv,bv);
        
        tblr = [tblr,rv,lastc]; %#ok<AGROW>
        
    end
    
    table{i} = tblr;
end

% Print to file
fid = fopen('table2.txt','w');
fprintf(fid,'%s \n',table{:});
fclose(fid);

