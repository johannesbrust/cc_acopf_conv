\documentclass[12pt,notitlepage]{article}

%% Language and font encodings
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{sectsty}
\usepackage{titlesec}
	\titleformat*{\section}{\normalfont}
	\titleformat*{\subsection}{\normalfont}
	\titleformat*{\subsubsection}{\normalfont\itshape}
\renewcommand{\thesection}{\Roman{section}.} 
\renewcommand{\thesubsection}{\Alph{subsection}.}
\renewcommand{\thesubsubsection}{\arabic{subsubsection}.}
\usepackage{fancyhdr}


%% Sets page size and margins
\usepackage[a4paper,top=2.54cm,bottom=2.54cm,left=2.54cm,right=2.54cm,marginparwidth=1.75cm]{geometry}

%% Useful packages
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
%\usepackage[colorlinks=true, allcolors=blue]{hyperref}
\usepackage{indentfirst}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{changepage}
\usepackage{enumitem}
\usepackage{authblk}
\usepackage[backend=bibtex,style=phys]{biblatex}
\usepackage[figurename=FIG.]{caption}
%\usepackage[tablename=TABLE.]{caption}
\captionsetup[table]{name=TABLE}
\renewcommand{\thetable}{\Roman{table}}
\usepackage{float}
\floatstyle{plaintop}
\restylefloat{table}

\usepackage{etoolbox}
\patchcmd{\subequations}{{1}}{{-1}}{}{}       % decrement the equation counter
\patchcmd{\subequations}{\alph}{.\arabic}{}{} % change display format of eq. counter
% remove blue text in citations
%\usepackage{hyperref}
%\hypersetup{colorlinks,%
%	citecolor=black,%
%	filecolor=black,%
%	linkcolor=black,%
%	urlcolor=black
%}
\usepackage{color}
%\usepackage{ulem}

\newcommand{\jjb}[1]{\textcolor{blue}{#1}}

\addbibresource{sources.bib}

\title{\vspace{4.5cm}Chance-constrained AC optimal power flow: An iterative reformulation with convergence analysis via the fixed-point method}

\date{August 2, 2019}

\author[$\dagger$]{Eric Grimm\thanks{\texttt{egrimm@rollins.edu}}}
\author[$\S$]{Johannes Brust}
\affil[$\dagger$]{Rollins College, Winter Park, FL}
\affil[$\S$]{MCS Division, Argonne National Laboratory}


\begin{document}

\maketitle
    
\begin{center}
Prepared in fulfillment of the requirement of the student research participation program at Argonne National Laboratory in Lemont, IL.%\vspace{1cm}
\end{center} 
 
    
    \begin{abstract}
    	\begin{spacing}{2}
    		\begin{adjustwidth}{-65pt}{-65pt}
		    \space Optimal power flow (OPF) models have been around for over 50 years. The OPF model is an optimization problem that utilizes constraints to model the physical limitations of components in the power grid. Until recently they have accurately modeled conventional power systems, but the escalating popularity of solar energy is changing this. Solar energy is unpredictable due to its reliance on sun exposure. In order to deal with this source of uncertainty, chance constrained alternating current OPF (CCACOPF) models have been developed. However, solving even relatively small chance constrained optimization problems is typically computationally challenging. Therefore, we describe a reformulated CCACOPF model, which addresses the difficulties of including chance constraints. In particular, our reformulation uses an iterative algorithmic scheme that scales to large problems, as well. The iterative approach transforms the probabilistic CCACOPF problem into a deterministic one. It does this by first solving a conventional ACOPF problem. Next, it updates a subset of the variable bounds that would represent chance constraints in the probabilistic model. Lastly, it resolves the ACOPF problem with updated bounds. The algorithm will terminate when the convergence criteria are met. Formulations of this iterative method have previously appeared in literature, but our method includes different chance constraints and by regarding the approach as a fixed-point iteration, we propose a set of conditions that determine the convergence of the method. We have applied our model to several test cases up to a 9241pegase case, which produced a result in 30 minutes on a standard laptop. Smaller cases such as a 118 or 1354 case can be solved in seconds. The model currently operates when solving cases with one reference bus, but it can be modified to include more. The CCACOPF model is part of a larger initiative to establish a more functional and efficient model for gas and power networks in the United States.		
		    \end{adjustwidth}	
	    \end{spacing}
    \end{abstract}
\pagebreak


%\setcounter{page}{2}


\section{Introduction}
The past few decades have seen the rise and maturity of the optimal power flow (OPF) field. OPF problems lie in the area of electric power systems engineering and they consist of rigorous constrained optimization problems involving high dimension and non-convex functions \supercite{7737062}. Specifically, OPF is a problem that wishes to optimize an electrical power system subjected to physical constraints required by engineering limits and electrical laws on certain variables \supercite{Frank2012APO}. In the present, this problem is becoming straightforward with conventional power grids involving energy from nuclear, coal, and natural gas production. The problem is deterministic and does not deal with probabilities, but this is changing.

% need to get some references in this paragraph
There has been a sharp rise in the production of solar and wind energy. Both of these new forms of energy cast uncertainties into the power grid. The models that we describe in this paper only take into account solar energy. Compared to conventional power generation, solar energy is unreliable and inconsistent. The solar demand and output are unpredictable and does not lend itself well to the OPF model. Thus, a chance-constrained ACOPF (CCACOPF) model must be used going forward if a power grid system has solar power generation. As the name suggests, this type of model includes probability factors into the problem's constraints.

Initial implementations of this method existed at Argonne National Laboratory (ANL), developed using Julia's JuMP package, but they proved to be inefficient and suffered from computational bottlenecks. The model was only able to solve a 30 bus case. Thus, the model was not scalable. The problem is that this model, we will refer to it as the ``one-shot approach,'' attempted to solve the chanced constrained problem all at once. Due to JuMP limitations and the approach as a whole, bottlenecks limited the feasibility of this one-shot approach. Instead, we have developed an iterative algorithm to solve the CCACOPF problem, which is adapted from previous work in the literature\supercite{8017474}. The iterative approach is advantageous because it transforms a probabilistic problem involving chance constraints into a deterministic one where the chance constraints are represented by uncertainty margins on variable bounds \supercite{8017474}. Unlike the one-shot approach, the iterative approach solves a sequence of simpler optimization problems. In particular, our version of this approach incorporates different constraints and improved justification for convergence using the fixed-point theorem.

%% Outline for Intro
%\begin{enumerate}
%	\item Background on ACOPF
%	\item Uncertainty with solar power
%	\item Chance constrained ACOPF
%	\begin{enumerate}
%		\item Include old formulation? or put in main body
%		\item work done at ANL?
%	\end{enumerate}
%	\item JuMP
%\end{enumerate}
\section{The Iterative Approach}
	\subsection{ACOPF Basis}
		\subsubsection{Variables and Equations}
		We consider a standard power grid that has $N$ total buses of three types, reference/slack buses, generator buses, and load buses. The indexing sets are $R$, $G$, and $L$ respectively. The set of all buses is $B = R \cup G \cup L$. In order to streamline the equations, we group the variables into the following vectors. The known variables compose the vectors $u$, $p$, and $d$. The unknown variables are are contained in vector $x$.
		\begin{align*}
		u :=
		\begin{bmatrix}
		P_{g}^G \\ V^G
		\end{bmatrix},
		p :=
		\begin{bmatrix}
		V^R \\ \theta^R
		\end{bmatrix},
		d :=
		\begin{bmatrix}
		P_{d}^L \\ Q_{d}^L \\ P_d^G \\ Q_d^G
		\end{bmatrix},
		x :=
		\begin{bmatrix}
		Q_g^G \\ Q_g^R \\ V^L \\ \theta^L \\ \theta^G
		\end{bmatrix}.
		\end{align*}
		Each also has the set of power balance equations:
		%\begin{subequations}
			\begin{align*}
			P^i(\theta, V) - P_{\text{net}}^i = 0 ,\quad i = 1\ldots N \\
			Q^i(\theta, V) - Q_{\text{net}}^i = 0 ,\quad i = 1\ldots N
			\end{align*}
		%\end{subequations}
		where
		%\begin{subequations}
			\begin{align*}
			P^i(\theta, V) := V^i \sum_{k=1}^N V^k \left( G_{ik} \cos(\theta^i - \theta^k) + B_{ik}\sin(\theta^i - \theta^k) \right) \\
			Q^i(\theta, V) := V^i \sum_{k=1}^N V^k \left( G_{ik} \sin(\theta^i - \theta^k) - B_{ik}\cos(\theta^i - \theta^k) \right). 
			\end{align*}
		%\end{subequations}
		These power balance equations are used in the following nonlinear system
		\begin{align}
		\label{powerflow}
		f(x; u, p) =
		\begin{bmatrix}
		P^L(x; u, p) - P^L_{\text{net}} \\
		Q^L(x; u, p) - Q^L_{\text{net}} \\
		P^G(x; u, p) - P^G_{\text{net}} \\
		Q^G(x; u, p) - Q^G_{\text{net}}
		\end{bmatrix}
		\end{align}
		which is used in the chance constrained problem formulations. The initial ACOPF and CCACOPF formulations at ANL did not make use of the $Q^G$ component, but the iterative reformulation does. A solution  point is found when $f(x; u, p) = 0$.
	
		\subsubsection{ACOPF}
		We will consider this formulation of the ACOPF problem
		
		\begin{subequations}
		\label{acopf}
			\begin{alignat}{3}
			\underset{P_g^G, V^G}{\min} & \quad && C(P_g^G) \\
			\text{s.t.} && \quad & f\left(x;u,p\right) = 0 \\
			&&& \theta^R := 0, \quad P^R (x; u, p) := P_{\text{net}}^R \\
			&&& \mathcal{A} =
			\begin{cases}
			V_{\min} \leq V^G \leq V_{\max} \\
			V_{\min} \leq V^R \leq V_{\max}
			\end{cases}\\
			&&& \mathcal{B} =
			\begin{cases}
			V_{\min} \leq V^L \leq V_{\max} \\
			\theta_{\min} \leq \theta^{L} \leq \theta_{\max} \\
			\theta_{\min} \leq \theta^{G} \leq \theta_{\max} \\
			Q_{g \space \min} \leq Q_g^G \leq Q_{g \space \max}
			\end{cases}
			\end{alignat}
		\end{subequations}
		however, there are alternate ways to construct the ACOPF problem. This will serve as a basis for the probabilistic CCACOPF, but will be a defining component of the iterative reformulation.

%% Outline for body
%\begin{enumerate}
%	\item Variables
%	\item Old CCACOPF?
%	\item gamma definition
%	\item new reformulation
%	\item lambda meaning
%	\item convergence criteria
%	\item Performance
%\end{enumerate}


	\subsection{Old CCACOPF Formulation}
	
	The previous CCACOPF model at ANL took on this structure
	
	%\begin{subequations}
		\label{cc-acopf1}
		\begin{alignat*}{3}
		\underset{P_g^G, V^G}{\min} & \quad && C(P_g^G) \\
		\text{s.t.} && \quad & f\left(x; u, p \right) = 0 \\
		&&& \theta^R := 0, \quad P^R (x; u, p) := P_{\text{net}}^R \\
		&&& \mathcal{A} =
		\begin{cases}
		V_{\min} \leq V^G \leq V_{\max} \\
		V_{\min} \leq V^R \leq V_{\max}
		\end{cases}\\
		&&& \mathcal{B} =
		\begin{cases}
		\mathbb{P} \left[ V_{\min}^i \leq V^{i} \right]  \geq 1 - \frac{\epsilon_V}{|L|}, \quad i \in L\\
		\mathbb{P} \left[ V_{\max}^i \geq V^{i} \right]  \geq 1 - \frac{\epsilon_V}{|L|}, \quad i \in L\\
		\mathbb{P} \left[ \theta_{\min}^i \leq \theta^{i} \right] \geq 1 - \frac{\epsilon_{\theta}}{|L|+|G|}, \quad i \in L \cup G \\
		\mathbb{P} \left[ \theta_{\max}^i \geq \theta^{i} \right] \geq 1 - \frac{\epsilon_{\theta}}{|L|+|G|}, \quad i \in L \cup G \\
		\mathbb{P} \left[ Q_{g \space \min}^i \leq Q_g^{i} \right]  \geq 1 - \frac{\epsilon_{Q_g}}{|G|}, \quad i \in G\\
		\mathbb{P} \left[ Q_{g \space \max}^i \geq Q_g^{i} \right] \geq 1 - \frac{\epsilon_{Q_g}}{|G|}, \quad i \in G
		\end{cases}
		\end{alignat*}
	%\end{subequations}
	where the $\mathcal{A}$ constraints are taken from ACOPF problem and the $\mathcal{B}$ constraints represent the chance constraints and are used to include solar demand uncertainty into the problem. They are similar to the ones in $\mathcal{B}$ from the ACOPF except they now include sensitivity adjustments and uncertainty. Constrained optimization problems are already difficult to solve and the addition of chance constraints only adds to this. The existing Julia JuMP model at ANL suffered from massive bottlenecks that rendered the model useless when applied to cases with more than 30 buses. The bottlenecks occured in the chance constrained variables. Some variable calculations involved references to other JuMP variables, which hindered the performance of the model greatly. The fact that JuMP constraints can only be expressed in terms of algrebric expressions also caused difficulties \supercite{dunning2017jump}. The ANL model implemented several work arounds for this issue that likely slowed down the model even more.
	
	\subsection{Iterative Reformulation}
	These difficulties led us to develop an iterative algorithm by referencing a technique first developed by Line Roald. Her reformulation of the problem involved conducting a series of ACOPF solves while adjusting chance constrained bounds based on sensitivity values from the latest solve \supercite{8017474}. Ours incorporates a similar approach, but involves constraints on different variables and a more robust technique for determining the convergence of the model.
		Our reformulation takes on this form
		\begin{subequations}
			\label{cc-acopf2}
			\begin{alignat}{3}
			\underset{P_g^G, V^G}{\min} & \quad && C(P_g^G) \\
			\text{s.t.} && \quad & f\left(x; u, p \right) = 0 \\
			&&& \theta^R := 0, \quad P^R (x; u, p) := P_{\text{net}}^R \\
			&&&V_{\min} \leq V^{G\cup R} \leq V_{\max} \\
			&&&V_{\min}^L + \lambda_V \leq V^L  \leq V_{\max}^L - \lambda_V\\
			&&&\theta_{\min}^{L\cup G} + \lambda_\theta \leq \theta^{L\cup G}  \leq \theta_{\max}^{L\cup G}  - \lambda_\theta\\
			&&&Q_{g \space \min}^G+ \lambda_{Q_g} \leq Q_g^G  \leq Q_{g \space \max}^G - \lambda_{Q_g}
			\end{alignat}
		\end{subequations}
	where the $\lambda$  values are
	\begin{subequations}
		\label{eq:lambdas}
		\begin{alignat}{3}
		\lambda_V &= \enspace \Phi^{-1}(1-\epsilon_V)\|e_i^{\top}\Gamma \Sigma \|_2,\enspace &i& = L\\
		\lambda_\theta &= \enspace \Phi^{-1}(1-\epsilon_\theta)\|e_i^{\top}\Gamma \Sigma \|_2,\enspace &i& = L\cup G\\
		\lambda_{Q_g} &= \enspace \Phi^{-1}(1-\epsilon_{Q_g})\|e_i^{\top}\Gamma \Sigma \|_2,\enspace &i& = G
		\end{alignat}
	\end{subequations}
	\supercite{8017474}.
	Every variable that was chance constrained in (\ref{cc-acopf1}) has its own $ \lambda $ value in this version. (3.5 - 3.7) represent the new chance constraints.
	
	(\ref{eq:lambdas}) demonstrates how the $\lambda$'s are computed. The $\Gamma$ matrix variable is derived from the Jacobian of the powerflow equations from (\ref{powerflow}) and accounts for solar uncertainty. For a solution point $x$ and some $y$ there is the implicit function $g: \mathbb{R}^{|y|}\to\mathbb{R}^{|x|}$ such that $f(g(y), y) = 0$ in an area around a power flow point, by the implicit function theorem. Let g(y) = x around a solution point, then the first two terms of the Taylor series of $g$ give a definition for $\Gamma$:
	\begin{align*}
	g(\hat{y}) \approx g(y) + \left[ -\left[ \frac{\partial f}{\partial x} \right]^{-1} \left[ \frac{\partial f}{\partial y} \right] \right] (\hat{y} - y) \implies \hat{x} \approx x + \Gamma (\hat{y} - y).
	\end{align*}
	In (\ref{eq:lambdas}) $\Phi^{-1}$ is the inverse cumulative distribution function evaluated at $1-\epsilon$ \supercite{frolov2019cloud} and $\Sigma$ represents the covariance matrix. Here $e_i^{\top}$ denotes the ith row of the identity matrix.

	\subsection{Iterative Algorithm Structure}
	The iterative CCACOPF is essentially an ACOPF problem, but it is solved multiple times with modified constraints each time. This method works since the influence of uncertainty in the problem is only seen in the unceratainty margins $\lambda$ \supercite{8017474}. The Julia model begins by solving cases based on the ACOPF model detailed in (\ref{acopf}), after solving this the $\Gamma$ sensitivity matrix is calculated based on the Jacobian calculations from the first ACOPF solve. The $\lambda$ values are now able to be calculated using $\Gamma$ and are added to the upper and lower bounds as (\ref{cc-acopf2}) illustrates. The model does not converge on the first iteration. Thus, the ACOPF problem is resolved using the new chance constraints from the previous iteration. The iteration will continue until the $\lambda$ values stop changing. To do this, we check to see if the maximum difference in the $\lambda$'s is within some tolerance $\eta$. This is illustrated in (FIG. 1).
	\begin{figure}[H]
		\centering
		 \includegraphics[width = .75 \textwidth]{figure1.pdf}
		 \label{figure1}
		 \caption{This demonstrates the algorithm used in the iterative CCACOPF. Initially, the ACOPF problem is solved, which then allows for the calculation of the $\lambda$ values to be used in the next solve \supercite{8017474}. The model convergences when the $\lambda$'s stop changing.}
	\end{figure}
	%\vspace{-.7cm}
\section{Model Behavior}

	\subsection{Convergence}
	
	\subsubsection{Fixed-point definition for CCACOPF}
		Previous implementations of this iterative approach were not gaurenteed to converge, but we have been able to relate the convergence of the model to the fixed-point method. For brevity, we have simplified the CCACOPF formulation to solve the convergence of the $\lambda$'s
		\begin{subequations}
			\label{eq:cc-acopf}
			\begin{alignat}{3}
			\underset{\hat{x}}{\min} & \quad && C(\hat{x}) \\
			\text{s.t.} && \quad & f(\hat{x}) = 0 \\
			&&&g_1(\hat{x}) \geq 0 \\
			&&&g_2(\hat{x}) + \lambda(\hat{x}) \geq 0.
			\end{alignat}
		\end{subequations}
		%\jjb{Perhaps we may relabel $f$ in the problem formulation from \label{eq:cc-acopf}.}
		where $\hat{x}$ is a representation of all variables in the original problem. The fixed-point method entails that a fixed point $x_*$ of the function $h$ satisfies the conditions $h(x_*) = x_*$ and $|h'(x_*)| \in [0,1)$ \supercite{strogatz}.
		Our CCACOPF model uses an iterative algorithm. $\lambda$ values calculated from the first iteration help determine the variables that will go into the next iteration. Thus, there should be some relationship between $\lambda$ and the other variables in the problem, which we refer to as $\hat{x}$. If we start at $\hat{x}_0$ and $k=0$:
		%\begin{subequations}
			\begin{equation}
			\label{fixedPointProof}
			\lambda_{k+1} = \lambda(\hat{x}_k), \quad \hat{x}_{k+1} = \hat{x}_f (\lambda_{k+1}) \quad \therefore \quad \lambda_{k+1} = \lambda(\hat{x}_f(\lambda_k)) = f_{\lambda}(\lambda_k).
			\end{equation}
		%\end{subequations}
		Claim: If $\lambda_k\rightarrow \lambda_*$, then $\hat{x}_k \rightarrow \hat{x}_*$. This relates the $\lambda$'s and the $\hat{x}$'s. If $\lambda$'s converge then the whole model will converge.
		
		Assume that $\hat{x}$ is a fixed point. The equation that we want to prove convergence for is:
		
		\begin{equation}
			\label{function}
			\lambda_i(\hat{x}(\lambda)) =  \Phi^{-1}(1-\epsilon)\| e_i^{\top}\Gamma(\hat{x}(\lambda)) \Sigma \|_2
		\end{equation}
		which is an altered version of (\ref{eq:lambdas}) to include the relationship between the $x$'s and the $\lambda$'s from (\ref{fixedPointProof}). The $\Gamma$ in this instance is defined as a function:
		\begin{equation}
		\Gamma(\hat{x}(\lambda)) = -J(\hat{x}(\lambda))^{-1}E.
		\end{equation}
		where the columns of E are selected columns of the identity matrix. For all $\lambda_i(x(\lambda))$ the derivative must be in between 0 and 1. In our case, we will take the norm of the Jacobian of the $\lambda$'s. If the maximum eigenvalue is within 0 and 1 then all the $\lambda$'s will convergence, which means that the whole problem will converge as shown in (\ref{fixedPointProof}).
	
	\subsubsection{Derivative test}
	
		To avoid clutter, we will be using the following definitions.
		
		\begin{equation}
		\label{eq:si}
		S_i (\hat{x}(\lambda)) =  \Sigma \Gamma(\hat{x}(\lambda))e_i
		\end{equation}
		\begin{equation}
		z_i = \Phi^{-1}(1-\epsilon)
		\end{equation}
		This leaves us with the modified function
		\begin{equation}
		\label{lambdadef2}
		\lambda_i(\hat{x}(\lambda)) = z_i[S_i (\hat{x}(\lambda))^\top S_i (\hat{x}(\lambda))]^{1/2}.
		\end{equation}
		We assume that $\lambda_i$ is differentiable if the Jacobian of the powerflow equations (\ref{powerflow}) is invertible. If we first take the partial derivative with respect to $\lambda_i$, then we have
		\begin{equation}
		\label{firstD}
		\frac{\partial \lambda_i}{\partial \lambda_i} = \frac{z_i^2}{\lambda_i}\left[ S_i^{\top}\frac{\partial S_i}{\partial \lambda_i}\right] 
		\end{equation}
		with some substitutions from (\ref{function}). This means that $\lambda_i \neq 0$ or $\epsilon \neq 0.5$. (\ref{firstD}) provides a basis for what the partial derivatives will look like. We can extend this to the full Jacobian
		
		\begin{equation*}
			\label{Jacob}	
			J_\lambda = 
			\begin{bmatrix}
			\frac{z_1^2}{\lambda_1}\left[ S_1^{\top}\frac{\partial S_1}{\partial \lambda_1}\right]& \frac{z_1^2}{\lambda_1}\left[ S_1^{\top}\frac{\partial S_1}{\partial \lambda_2}\right]&\dots& \frac{z_1^2}{\lambda_1}\left[ S_1^{\top}\frac{\partial S_1}{\partial \lambda_j}\right]\\
			\frac{z_2^2}{\lambda_2}\left[ S_2^{\top}\frac{\partial S_2}{\partial \lambda_1}\right]& \frac{z_2^2}{\lambda_2}\left[ S_2^{\top}\frac{\partial S_2}{\partial \lambda_2}\right]&\dots& \frac{z_2^2}{\lambda_1}\left[ S_2^{\top}\frac{\partial S_2}{\partial \lambda_j}\right]\\
			\vdots&\vdots&\ddots&\vdots\\
			\frac{z_i^2}{\lambda_i}\left[ S_i^{\top}\frac{\partial S_i}{\partial \lambda_1}\right]&
			\frac{z_i^2}{\lambda_i}\left[ S_i^{\top}\frac{\partial S_i}{\partial \lambda_2}\right]&\dots&
			\frac{z_i^2}{\lambda_i}\left[ S_i^{\top}\frac{\partial S_i}{\partial \lambda_j}\right]
			\end{bmatrix}.
		\end{equation*}
		
		This can be split up into three matrices. A $Z_2$ matrix would be a diagonal matrix with values of $z_i^2$. A $\lambda_R$ matrix would be a diagonal matrix with $\frac{1}{\lambda_i}$ values. Lastly, an $S_\lambda$ matrix would contain the rest of the entries in $J_\lambda$, the $S_i$'s and the partials.
		\begin{equation*}
		J_\lambda = Z_2 \lambda_R S_{\lambda}
		\end{equation*}
		\begin{equation*}
			\label{splitJacob}
			J_\lambda = 
			\begin{bmatrix}
			z_1^2 & & & \\
			& z_2^2 & & \\
			& & \ddots & \\
			& & & z_i^2
			\end{bmatrix}
			\begin{bmatrix}
			\frac{1}{\lambda_1} & & & \\
			& \frac{1}{\lambda_2} & & \\
			& & \ddots & \\
			& & & \frac{1}{\lambda_i}
			\end{bmatrix}
			\begin{bmatrix}
			S_1^{\top}\frac{\partial S_1}{\partial \lambda_1}& S_1^{\top}\frac{\partial S_1}{\partial \lambda_2}&\dots& S_1^{\top}\frac{\partial S_1}{\partial \lambda_j}\\
			S_2^{\top}\frac{\partial S_2}{\partial \lambda_1}& S_2^{\top}\frac{\partial S_2}{\partial \lambda_2}&\dots& S_2^{\top}\frac{\partial S_2}{\partial \lambda_j}\\
			\vdots&\vdots&\ddots&\vdots\\
			S_i^{\top}\frac{\partial S_i}{\partial \lambda_1}&
			S_i^{\top}\frac{\partial S_i}{\partial \lambda_2}&\dots&
			S_i^{\top}\frac{\partial S_i}{\partial \lambda_j}
			\end{bmatrix}
		\end{equation*}
		Each entry of the $S_{\lambda}$ is a dot product since $S_i^{\top}$ is a row vector and $\frac{\partial S_i}{\partial \lambda_j}$ is a column vector. Hence, every entry can be written as
		\begin{equation}
			\label{dotproduct}
			S_i^{\top}\frac{\partial S_i}{\partial \lambda_j} = \left\| S_i\right\|_2 \left\| \frac{\partial S_i}{\partial \lambda_j}\right \|_2\cos(\hat{\theta}_{ij}).
		\end{equation}
		From (\ref{lambdadef2}) $\lambda_i = z_i \left\| S_i \right\|_2$, which means that  $\left\| S_i \right\|_2 = \frac{\lambda_i}{z_i}$. If we substitute this into (\ref{dotproduct}), then the equation becomes
		\begin{equation}
			\label{dotproduct2}
			S_i^{\top}\frac{\partial S_i}{\partial \lambda_j} = \frac{\lambda_i}{z_i} \left\| \frac{\partial S_i}{\partial \lambda_j}\right \|_2\cos(\hat{\theta}_{i,j}).
		\end{equation}
		Thus, if we substitute (\ref{dotproduct2}) into the entries of the $S_{\lambda}$ matrix then the $\lambda$'s in $S_{\lambda}$ will cancel out with the $\lambda_R$ matrix and the $z_i$'s in $Z_2$ will no longer be squared since the $z_i$'s cancel. The Jacobian can now be represented as
		\begin{equation}
			\label{splitJacob2}
			J_\lambda = 
			\begin{bmatrix}
			z_1 & & & \\
			& z_2 & & \\
			& & \ddots & \\
			& & & z_i
			\end{bmatrix}
			\begin{bmatrix}
			\left\| \frac{\partial S_1}{\partial \lambda_1}\right\|_2\cos(\hat{\theta}_{1,1})& \left\| \frac{\partial S_1}{\partial \lambda_2}\right\| _2\cos(\hat{\theta}_{1,2})&\dots& \left\| \frac{\partial S_1}{\partial \lambda_j}\right\|_2\cos(\hat{\theta}_{1,j})\\
			\left\| \frac{\partial S_2}{\partial \lambda_1}\right\|_2\cos(\hat{\theta}_{2,1})& \left\| \frac{\partial S_2}{\partial \lambda_2}\right\|_2\cos(\hat{\theta}_{2,2})&\dots& \left\| \frac{\partial S_2}{\partial \lambda_j}\right\|_2\cos(\hat{\theta}_{2,j})\\
			\vdots&\vdots&\ddots&\vdots\\
			\left\| \frac{\partial S_i}{\partial \lambda_1}\right\|_2\cos(\hat{\theta}_{i,1})&
			\left\| \frac{\partial S_i}{\partial \lambda_2}\right\|_2\cos(\hat{\theta}_{i,2})&\dots&
			\left\| \frac{\partial S_i}{\partial \lambda_j}\right\|_2\cos(\hat{\theta}_{i,j})
			\end{bmatrix}
		\end{equation}
		
		Substituting (\ref{eq:si}) into $J_{\lambda}$, the entries of the second matrix are now
		\begin{equation}
			\label{SiToGamma}
			\left\| \frac{\partial S_i}{\partial \lambda_j}\right\|_2 \cos(\hat{\theta}_{i,j}) = \left\| \Sigma \frac{\partial \Gamma^{\top}}{\partial \lambda_j} e_i\right\|_2 \cos(\hat{\theta}_{i,j}). %\leq \left\| \Sigma \right\|_2 \left\|\frac{\partial %\Gamma^{\top}}{\partial \lambda_j} e_i\right\|_2 \cos(\hat{\theta}_{i,j}).
		\end{equation} 
		In this equation, $\frac{\partial \Gamma^{\top}}{\partial \lambda_j} e_i$ represents the partial derivative of the inverted Jacobian of the power flow equations from (\ref{powerflow}). In (\ref{eq:cc-acopf}) all variables in the original chance constrained problem are represented as $\hat{x}$ and all $\lambda$ constraints have the appearance $L_i + \lambda_i \leq \hat{x} \leq U_i - \lambda_i$ where $L_i$ and $U_i$ are lower and upper bounds respectively. In order to reach a solution point, all $\hat{x}$'s representing chance constraints must be within these bounds. Meaning that with each iteration, each $x$ can change, but it can only change within the confines of the bounds. Since the bounds are finite, the change in the $\hat{x}$'s with respect to the $\lambda$'s is also finite. Since the change in the $\hat{x}$'s is finite, the changes in the inverted Jacobian of the power flow equations would be as well. Thus, $\frac{\partial \Gamma^{\top}}{\partial \lambda_j} e_i$ is finite. (\ref{SiToGamma}) can be split up into individual norms which means
		\begin{equation*}
			\left\| \Sigma \frac{\partial \Gamma^{\top}}{\partial \lambda_j} e_i\right\|_2 \cos(\hat{\theta}_{i,j}) \leq \left\| \Sigma \right\|_2 \left\|\frac{\partial \Gamma^{\top}}{\partial \lambda_j} e_i\right\|_2 \cos(\hat{\theta}_{i,j}).
		\end{equation*}
		Now, we can think of $\left\|\frac{\partial \Gamma^{\top}}{\partial \lambda_j} e_i\right\|_2 \cos(\hat{\theta}_{i,j})$ as being some finite constant $K$. A bound on the spectral norm of the Jacobian is now obtained as
		\begin{equation}
			\label{splitJacob3}
			\left \| J_\lambda \right \|_2 \le
			\left \|
			\begin{bmatrix}
			z_1 & & & \\
			& z_2 & & \\
			& & \ddots & \\
			& & & z_i
			\end{bmatrix}
			\right \|_2
			\left \|
			\begin{bmatrix}
			\left\|\Sigma \right\|_2 K_{1,1}& \left\| \Sigma \right\| _2 K_{1,2}&\dots& \left\| \Sigma \right\|_2 K_{1,j}\\
			\left\|\Sigma \right\|_2 K_{2,1}& \left\| \Sigma \right\| _2 K_{2,2}&\dots& \left\| \Sigma \right\|_2 K_{2,j}\\
			\vdots&\vdots&\ddots&\vdots\\
			\left\|\Sigma \right\|_2 K_{i,1}& \left\| \Sigma \right\| _2 K_{i,2}&\dots& \left\| \Sigma \right\|_2 K_{i,j}
			\end{bmatrix}
			\right \|_2.
		\end{equation}
		
		%	\jjb{Perhaps eventually we may use the identity:$ \left[ S_i^{\top}\frac{\partial S_i}{\partial \lambda_j}\right] = \| S_i \|_2 \| \frac{\partial S_i}{\partial \lambda_j} \|_2 \cos(\theta_{ij}) $, where $ \theta_{ij} $ represents the angle 
		%		between the two vectors on the left of the equality and $ | \cos(\theta_{ij})| \le 1 $.}
		(\ref{splitJacob3}) is an $i$ by $j$ square matrix, which means that we can compute eigenvalues of $J_\lambda$, which would correspond to the 2-norm. The largest eigenvalue will be used for the derivative test. Even though we split up the Jacobian, we can multiply the largest eigenvalues of the two matrices to get an upper bound on the largest eigenvalue of the whole Jacobian. Since the $z$'s and $K$'s are finite entries, if $\Sigma$ is sufficiently small then $||J_\lambda||_2 \in [0,1)$. Thus, the model converges.
		
		\subsection{Performance}	
		The iterative model produces much faster solution times than the one-shot approach, which we will call model B. Model B was unable to solve cases beyond 30 buses, but with our testing of the iterative model, model C, we were able to solve up to a 9241pegase case, an impressive improvement. The objective function value was similar between the two models. We did testing on the 30 bus case since that is the highest case that model B can handle (FIG. 2). Overall the two models performed similarly, which implies that model C can be used going forward. However, (FIG. 2) shows little variation between the cost of the ACOPF and the CCACOPF models. Generally, CCACOPF a cost increase of a few percentage points \supercite{bienstock2014chance}, but the increase that we observe is minimal in (FIG. 2). We conducted the experiment with the same $\Sigma$ matrix value, which was set to be quite small. Moreover, when we varied $\Sigma$ in the 30 bus case we found that as $\Sigma$ increases then the cost increases as well (FIG. 3). This means that with the correct covariance value, then the iterative CCACOPF should output a more reasonable cost value.
		\begin{figure}[H]
			\centering
			\includegraphics[width = .8 \textwidth]{test1.png}
			\label{figure2}
			\caption{We compare the cost of 3 different models, the traditional ACOPF, model B, and model C when $\epsilon_V$ is varied. As expected, the ACOPF does not change since it is not chanced constrained, but the others do. Model B and model C appear to perform similarly with model C costing slightly more with a lower $\epsilon_V$ value.}
		\end{figure}

		\begin{figure}[H]
		 	\centering
		 	\includegraphics[width = .8 \textwidth]{test2.png}
		 	\label{figure3}
		 	\caption{This shows the relationship between cost and the value of $\Sigma$. As $\Sigma$ increases the cost increases as well. At the highest cost, model C costs about 6\% more than the baseline ACOPF.}
		\end{figure}
		
\section{Conclusion}
	We developed the iterative CCACOPF to provide a work around for the bottlenecked one-shot model at ANL. The development of these models is part of larger push to provide large scale optimization techniques to power grids in the US to deal with the rise in solar power. We were able to adapt previous work done by Line Roald into our own iterative CCACOPF model and provide a reasonable justification for why and when the model converges through the fixed-point method.
	
	The original implementations of CCACOPF models at ANL were inefficient, but seemed to produce reliable results. With the iterative approach, we were able to produce a model that retained similar results that excelled in efficiency. Case 30 was the limit for the old versions of the model, but our version can solve cases with over 9000 buses. Despite this, the solution times do grow as the number of buses go up. The 9241pegase case takes 30 minutes on a standard laptop, which is not ideal. The problem is likely due to inefficiencies in the Jacobian and $\Gamma$ calculations. This would be something to improve upon going forward. The lack of support for cases with more than one reference bus is another problem with the model that should be added in the future to make it even more robust. Additionally, more testing could be done on the feasibility of the iterative approach. This could be done by further comparing model B and model C.
	
	The conclusion that we arrive at for convergence of the model is satisfactory, but it is possible that more could be done with this proof. For instance, a more clear definition for $\frac{\partial S_i}{\partial \lambda_j}$ terms. In our analysis we were able to conclude that these values are finite and can thus be decreased by applying a sufficiently small $\Sigma$ matrix. Through both experimental observation and theoretical analysis, the $\lambda$'s are shown to converge, but a more formal and elegant justification may exist. 
	
	It is possible that the iterative approach to the ACOPF model could be extended to security constrained OPF, which is a more demanding problem than the CCACOPF problem. Also, there are plans to incorporate load dynamics into the CCACOPF model. This will be beneficial since solar energy is not the only cause of uncertainty in the grid. The composition of electronics using power from the grid also plays a key role in operation and uncertainties. Adding this component to our CCACOPF model would increase its accuracy in modeling power grids. The CCAPCOPF model is an ongoing project at ANL as it is part of a larger initiative to optimize the grid and will continue to see improvement. 

\section*{Acknowledgements}
    I would like to express my thanks to the U.S. Department of Energy Office of Science and Office of Workforce Development for Teachers and Scientists for supporting me throughout this internship. I would also like to express my deepest appreciation to Jake Roth who assisted with this project and report. The completion of this project was also made possible by Mihai Anitescu who oversees the power grid optimization project developments.
    
    \appendix
    \section*{APPENDIX: Definition of OPF Variables}
    %turn this into a table
    All buses have the following variables included in Table I. The variables included in $u$ are controllable variables, $p$ variables are fixed variables, and $d$ contains the uncertain paramters caused by solar demand.
     % put net equations in description
			\begin{table}[H]
			\centering
			\begin{tabular}{ |l|c| }
				\hline
				Variable& Description  \\ \hline
				$P_g^i$& controllable active power which is defined to be zero for $i \in L$ \\ \hline
				$P_d^i$& fixed active power \\ \hline
				$P_{\text{net}}^i$& $:= P_g^i - P_d^i$: net active power \\ \hline
				$Q_g^i$& controllable reactive power which is defined to be zero for $i \in L$ \\ \hline
				$Q_d^i$&  fixed reactive power\\ \hline
				$Q_{\text{net}}^i$& $:= Q_g^i - Q_d^i$: net reactive power \\ \hline
				$V^i$& voltage magnitude at bus $i$ \\ \hline
				$\theta^i$& voltage angle at bus $i$ \\ \hline
			\end{tabular}
			\caption{This describes the meaning of the OPF variables. All variables are present at every bus.}
			\end{table}

\printbibliography

\end{document}